from django.urls import path

from . import views

app_name = 'paketMakan'

urlpatterns = [
    path('listPaketMakan/', views.listPaketMakan, name='listPaketMakan'),
    path('buatPaketMakan/', views.buatPaketMakan, name='buatPaketMakan'),
]
