from django.shortcuts import render

from database import MyDatabase

def listPaketMakan(request):
    db = MyDatabase()
    context = {}
    query = "SELECT * FROM PAKET_MAKAN"
    context["datas"] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'paketMakan/list_paket_makan.html', context)

def buatPaketMakan(request):
    db = MyDatabase()
    context = {}
    query = f"SELECT kode FROM HOTEL"
    context['datas'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'paketMakan/buat_paket_makan.html', context)