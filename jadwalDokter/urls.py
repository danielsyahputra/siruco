from django.urls import path

from . import views

app_name = 'jadwalDokter'

urlpatterns = [
    path('daftarJadwal/', views.daftar_jadwal, name='daftarJadwal'),
    path('listJadwal/', views.list_jadwal, name='listJadwal')
]