from django.shortcuts import render

from database import MyDatabase

def daftar_jadwal(request):
    db = MyDatabase()
    query = f"""
    SELECT kode_faskes, shift, tanggal
    FROM JADWAL
    WHERE (kode_faskes, shift, tanggal) NOT IN (
        SELECT kode_faskes, shift, tanggal 
        FROM JADWAL_DOKTER NATURAL JOIN DOKTER 
        where username = '{request.session['username']}'
    );
    """
    jadwals = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'jadwalDokter/daftar_jadwal.html', {"jadwals": jadwals})

def list_jadwal(request):
    return render(request, 'jadwalDokter/list_jadwal.html')