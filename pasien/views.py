from django.shortcuts import render
from database import MyDatabase

def daftar_pasien(request):
    return render(request, 'pasien/daftar_pasien.html')

def list_pasien(request):
    db = MyDatabase()
    username = request.session['username']
    query = f"SELECT nik, nama FROM PASIEN WHERE idpendaftar='{username}'"
    result = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'pasien/list_pasien.html', {"dataPasien": result})

def edit_pasien(request, pk):
    db = MyDatabase()
    query = f"SELECT nik, nama FROM PASIEN WHERE nik='{pk}'"
    result = db.query(query=query, fetchOne=True)
    db.close()
    context = {"nik": result[0], "nama": result[1]}
    return render(request, 'pasien/edit_pasien.html', context)