from django.urls import path

from . import views

app_name = 'pasien'

urlpatterns = [
    path('daftar_pasien/', views.daftar_pasien, name='daftar_pasien'),
    path('list_pasien/', views.list_pasien, name='list_pasien'),
    path('edit_pasien/<str:pk>/', views.edit_pasien, name='edit_pasien'),
]