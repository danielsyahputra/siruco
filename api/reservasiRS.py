from database import MyDatabase
from django.db import connection
from django.http.response import JsonResponse
from django.db.utils import IntegrityError

def reservasiRSAPI(request):
    if request.method == "POST":
        nik = request.POST['nik']
        kodeRS = request.POST['kodeRS']
        tanggal_masuk = request.POST['tanggal_masuk']
        tanggal_keluar = request.POST['tanggal_keluar']
        kodeRuangan = request.POST['kodeRuangan']
        kodeBed = request.POST['kodeBed']
        reservasi = (nik, tanggal_masuk, tanggal_keluar, kodeRS, kodeRuangan, kodeBed)
        try:
            with connection.cursor() as cursor:
                query = f"INSERT INTO RESERVASI_RS VALUES{str(reservasi)}"
                cursor.execute(query)
                return JsonResponse({"message": "Reservasi RS telah berhasil dibuat..."})
        except IntegrityError:
            return JsonResponse({"message": f"Reservasi RS dengan NIK Pasien {nik} pada tanggal {tanggal_masuk} sudah pernah dibuat..."})

def kodeRuanganAPI(request):
    if request.method == "GET":
        db = MyDatabase()
        kodeRS = request.GET['kodeRS']
        query = f"SELECT koderuangan FROM RUANGAN_RS WHERE koders='{kodeRS}'"
        result = db.query(query=query, fetchAll=True)
        db.close()
        return JsonResponse({"data": result}, safe=False)

def kodeBedAPI(request):
    if request.method == "GET":
        db = MyDatabase()
        kodeRS = request.GET['kodeRS']
        kodeRuangan = request.GET['kodeRuangan']
        query = f"SELECT kodebed FROM BED_RS WHERE koders='{kodeRS}' AND koderuangan='{kodeRuangan}'"
        result = db.query(query=query, fetchAll=True)
        db.close()
        return JsonResponse({"data": result}, safe=False)

def updateReservasiRSAPI(request):
    if request.method == "POST":
        nik = request.POST['nik']
        tanggal_masuk = request.POST['tanggal_masuk']
        tanggal_keluar = request.POST['tanggal_keluar']
        with connection.cursor() as cursor:
            query = f"""
            UPDATE RESERVASI_RS SET tglkeluar='{tanggal_keluar}'
            WHERE kodepasien='{nik}' AND
            tglmasuk='{tanggal_masuk}'
            """
            cursor.execute(query)
            return JsonResponse({"message": f"Reservasi RS oleh pasien dengan NIK {nik} pada tanggal {tanggal_masuk} telah diperbaharui..."})

def deleteReservasiAPI(request):
    nik = request.GET['nik']
    tanggal_masuk = request.GET['tanggal_masuk']
    try:
        with connection.cursor() as cursor:
            query = f"DELETE FROM TRANSAKSI_RS WHERE kodepasien='{nik}' AND tglmasuk='{tanggal_masuk}'"
            cursor.execute(query)
            query = f"DELETE FROM RESERVASI_RS WHERE kodepasien='{nik}' AND tglmasuk='{tanggal_masuk}'"
            cursor.execute(query)
            return JsonResponse({"message": f"Reservasi RS dari pasien dengan NIK Pasien {nik} pada tanggal {tanggal_masuk} berhasil dihapus..."})
    except:
        return JsonResponse({"message": "Sistem sedang gangguan..."})
