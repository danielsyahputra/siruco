from database import MyDatabase
from django.db import connection
from django.http.response import JsonResponse
from django.db.utils import IntegrityError

def pasienAPI(request):
    db = MyDatabase()
    if request.method == "GET":
        nik = request.GET['nik']
        query = f"SELECT * FROM PASIEN WHERE nik='{nik}'"
        result = db.query(query=query, fetchOne=True)
        db.close()
        return JsonResponse({"data": list(result)}, safe=False)
    elif request.method == "POST":
        pendaftar = request.session['username']
        nik = request.POST['nik']
        nama = request.POST['nama']
        no_telp = request.POST['no_telp']
        no_hp = request.POST['no_hp']
        jalan_ktp = request.POST['jalan_ktp']
        kel_ktp = request.POST['kel_ktp']
        kec_ktp = request.POST['kec_ktp']
        kab_ktp = request.POST['kab_ktp']
        prov_ktp = request.POST['prov_ktp']
        jalan_dom = request.POST['jalan_dom']
        kel_dom = request.POST['kel_dom']
        kec_dom = request.POST['kec_dom']
        kab_dom = request.POST['kab_dom']
        prov_dom = request.POST['prov_dom']
        try:
            with connection.cursor() as cursor:
                pasien = (nik, pendaftar, nama, jalan_ktp, kel_ktp, kec_ktp, kab_ktp, prov_ktp, jalan_dom, kel_dom, kec_dom, kab_dom, prov_dom, no_telp, no_hp)
                query = f"INSERT INTO PASIEN VALUES{str(pasien)}"
                cursor.execute(query)
                return JsonResponse({"status": 200,"message": "Data pasien telah diterima..."}, safe=False)
        except IntegrityError:
            return JsonResponse({"status": 500, "message": f"Pasien dengan NIK = '{nik}' telah terdaftar"})
    return JsonResponse({"message": "Sistem sedang gangguan, silahkan coba lagi nanti..."})

def deletePasienAPI(request):
    nik = request.GET["nik"]
    with connection.cursor() as cursor:
        cursor.execute(f"DELETE FROM PASIEN WHERE nik='{nik}'")
    return JsonResponse({"message": "Data telah dihapus..."})

def editPasienAPI(request):
    if request.method == "POST":
        query = "UPDATE pasien SET "
        nik = request.POST['nik']
        no_telp = request.POST['no_telp']
        no_hp = request.POST['no_hp']
        jalan_ktp = request.POST['jalan_ktp']
        kel_ktp = request.POST['kel_ktp']
        kec_ktp = request.POST['kec_ktp']
        kab_ktp = request.POST['kab_ktp']
        prov_ktp = request.POST['prov_ktp']
        jalan_dom = request.POST['jalan_dom']
        kel_dom = request.POST['kel_dom']
        kec_dom = request.POST['kec_dom']
        kab_dom = request.POST['kab_dom']
        prov_dom = request.POST['prov_dom']
        labels = ('ktp_jalan', 'ktp_kelurahan', 
            'ktp_kecamatan', 'ktp_kabkot', 
            'ktp_prov', 'dom_jalan', 
            'dom_kelurahan', 'dom_kecamatan', 
            'dom_kabkot', 'dom_prov',
            'notelp', 'nohp'
        )
        values = (jalan_ktp, kel_ktp, 
            kec_ktp, kab_ktp, 
            prov_ktp, jalan_dom, 
            kel_dom, kec_dom, 
            kab_dom, prov_dom, 
            no_telp, no_hp
        )
        query = "UPDATE PASIEN SET "
        with connection.cursor() as cursor:
            for i in range(len(labels) - 1):
                query += f"{labels[i]}='{values[i]}',"
            query += f"{labels[-1]}='{values[-1]}' WHERE nik='{nik}'"
            cursor.execute(query)
            return JsonResponse({"message": "Data sudah diperbaharui..."})
    return JsonResponse({"message": "Sistem sedang gangguan, silahkan coba lagi nanti..."})