from database import MyDatabase
from django.db import connection
from django.http.response import JsonResponse

def transaksiRSAPI(request):
    if request.method == "POST":
        idTransaksi = request.POST["idTransaksi"]
        tanggalBayar = request.POST["tanggalBayar"]
        waktuBayar = request.POST["waktuBayar"]
        status = request.POST['status']
        with connection.cursor() as cursor:
            query = f"""
            UPDATE TRANSAKSI_RS 
            SET statusbayar='{status}',
            tanggalpembayaran='{tanggalBayar}',
            waktupembayaran='{waktuBayar}'
            WHERE idtransaksi='{idTransaksi}'
            """
            cursor.execute(query)
            return JsonResponse({"message": f"Transaksi RS {idTransaksi} berhasil diperbaharui..."})
    else:
        idTransaksi = request.GET['idTransaksi']
        with connection.cursor() as cursor:
            query = f"DELETE FROM TRANSAKSI_RS WHERE idtransaksi='{idTransaksi}'"
            cursor.execute(query)
            return JsonResponse({"message": f"Transaksi RS {idTransaksi} berhasil dihapus..."})