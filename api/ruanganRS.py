from database import MyDatabase
from django.db import connection
from django.http.response import JsonResponse

from .helper import generateKodeBed, generateKodeRuangan

def ruanganRSAPI(request):
    with connection.cursor() as cursor:
        if request.method == "POST":
            kodeRS = request.POST['kodeRS']
            kodeRuangan = request.POST['kodeRuangan']
            tipe = request.POST['tipe']
            harga = request.POST['harga']
            jumlahBed = 0
            ruanganRS = (kodeRS, kodeRuangan, tipe, jumlahBed, harga)
            query = f"INSERT INTO RUANGAN_RS VALUES{str(ruanganRS)}"
            cursor.execute(query)
            return JsonResponse({"message": "Ruangan RS telah berhasil dibuat..."})
        else:
            kodeRS = request.GET['kodeRS']
            query = f"SELECT koders FROM RUANGAN_RS WHERE koders='{kodeRS}' ORDER BY koders DESC LIMIT 1"
            cursor.execute(query)
            result = cursor.fetchone()[0]
            kodeRuangan = generateKodeRuangan(result)
            return JsonResponse({"kodeRuangan": kodeRuangan}, safe=False)

def ruanganRSAPI2(request):
    with connection.cursor() as cursor:
        if  request.method == "POST":
            kodeRS = request.POST['kodeRS']
            kodeRuangan = request.POST['kodeRuangan']
            tipe = request.POST['tipe']
            harga = request.POST['harga']
            query = f"UPDATE RUANGAN_RS SET tipe='{tipe}', harga='{harga}' WHERE koders='{kodeRS}' AND koderuangan='{kodeRuangan}'"
            cursor.execute(query)
            return JsonResponse({"message": "Data ruangan RS telah diperbaharui..."})


def bedAPI(request):
    if request.method == "POST":
        kodeRS = request.POST['kodeRS']
        kodeRuangan = request.POST['kodeRuangan']
        kodeBed = request.POST['kodeBed']
        bed_rs = (kodeRuangan, kodeRS, kodeBed)
        with connection.cursor() as cursor:
            query = f"INSERT INTO BED_RS VALUES{str(bed_rs)}"
            cursor.execute(query)
            return JsonResponse({"message": "Bed RS telah berhasil dibuat..."})
    else:
        db = MyDatabase()
        kodeRS = request.GET['kodeRS']
        query = f"SELECT koderuangan FROM RUANGAN_RS WHERE koders='{kodeRS}'"
        kodeRuangan = db.query(query=query, fetchAll=True)
        db.close()
        return JsonResponse({"kodeRuangan": kodeRuangan}, safe=False)

def kodeBedAPI(request):
    kodeRS = request.GET['kodeRS']
    kodeRuangan = request.GET['kodeRuangan']
    kodeBed = generateKodeBed(kodeRS=kodeRS, kodeRuangan=kodeRuangan)
    return JsonResponse({"kodeBed": kodeBed})

def deleteBedAPI(request):
    kodeRS = request.GET['kodeRS']
    kodeRuangan = request.GET['kodeRuangan']
    kodeBed = request.GET['kodeBed']
    with connection.cursor() as cursor:
        query = f"DELETE FROM BED_RS WHERE koderuangan='{kodeRuangan}' AND koders='{kodeRS}' AND kodebed='{kodeBed}'"
        cursor.execute(query)
        return JsonResponse({"message": "Data bed telah behasil dihapus..."})