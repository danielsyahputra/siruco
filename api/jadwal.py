from database import MyDatabase
from django.db import connection
from django.http.response import JsonResponse

def jadwalDokterAPI(request):
    username = request.session['username']
    if request.method == "POST":
        nostr = request.session['nostr']
        jumlahPasien = 0
        kode_faskes = request.POST['kode_faskes']
        shift = request.POST['shift']
        tanggal = request.POST['tanggal']
        with connection.cursor() as cursor:
            jadwal_dokter = (nostr, username, kode_faskes, shift, tanggal, jumlahPasien)
            query = f"INSERT INTO JADWAL_DOKTER VALUES{str(jadwal_dokter)}"
            cursor.execute(query)
            return JsonResponse({"message": "Jadwal dokter telah dibuat."})
    elif request.method == "GET":
        db = MyDatabase()
        peran = request.session['peran']
        if peran == 'pengguna_publik' or peran == 'admin_satgas':
            query = f"SELECT * FROM JADWAL_DOKTER"
            result = db.query(query=query, fetchAll=True)
            db.close()
            return JsonResponse(
                {"data": result, "message": "Data telah didapatkan..."},
                safe=False,
                json_dumps_params={'indent': 2})
        else:
            nostr = request.session['nostr']
            query = f"SELECT * FROM JADWAL_DOKTER WHERE username='{username}' and nostr='{nostr}'"
            result = db.query(query=query, fetchAll=True)
            db.close()
            return JsonResponse(
                {"data": result, "message": "Data telah didapatkan..."}, 
                safe=False,
                json_dumps_params={'indent': 2})
    return JsonResponse({"message": "Sistem sedang gangguan, coba lagi nanti."}, safe=False)
