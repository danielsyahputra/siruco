from django.db.utils import IntegrityError
from django.db import connection
from django.http.response import JsonResponse
from django.db.utils import IntegrityError

def rumahSakitAPI(request):
    with connection.cursor() as cursor:
        if request.method == "POST":
            kodeFaskes = request.POST['kodeFaskes']
            rujukan = "1" if "rujukan" in request.POST.keys() else "0"
            try:
                query = f"INSERT INTO RUMAH_SAKIT VALUES('{kodeFaskes}', '{rujukan}')"
                cursor.execute(query)
                return JsonResponse({"message": "Rumah sakit telah behasil dibuat..."})
            except IntegrityError:
                return JsonResponse({"message": "Rumah sakit sudah pernah dibuat."})
        else:
            kodeFaskes = request.GET['kodeFaskes']
            rujukan = "1" if "rujukan" in request.GET.keys() else "0"
            query = f"UPDATE RUMAH_SAKIT SET isrujukan='{rujukan}' WHERE kode_faskes='{kodeFaskes}'"
            cursor.execute(query)
            return JsonResponse(
                {"message": f"Rumah sakit dengan kode {kodeFaskes} telah berhasil diperbaharui...", 
                "rujukan": rujukan})
