from django.db import connection
from django.http.response import JsonResponse

from .helper import generateKodeTRM, ambilKodeHotel

def transaksiMakanAPI(request):
    if request.method == "GET":
        idTransaksi = request.GET['idt']
        idTRM = generateKodeTRM(idTransaksi)
        kodeHotel = ambilKodeHotel(idTransaksi)
        result = []
        with connection.cursor() as cursor:
            query = f"SELECT kodepaket FROM PAKET_MAKAN WHERE kodehotel='{kodeHotel}'"
            cursor.execute(query)
            result = cursor.fetchall()
        return JsonResponse({"idTRM": idTRM, "kodeHotel": kodeHotel, "data": result}, safe=False)
    else:
        try:
            kodePaket = request.POST["kodePaket"].split("_")
            kodeHotel = request.POST["kodeHotel"]
            idt = request.POST["idt"]
            idTRM = request.POST["idTRM"]
            transaksiMakan = (idt, idTRM)
            with connection.cursor() as cursor:
                query = f"INSERT INTO TRANSAKSI_MAKAN VALUES{str(transaksiMakan)}"
                cursor.execute(query)
                for id_pesanan in range(0, len(kodePaket)):
                    daftar_pesan = (idTRM, id_pesanan + 1, idt, kodeHotel, kodePaket[id_pesanan])
                    query = f"INSERT INTO DAFTAR_PESAN VALUES{str(daftar_pesan)}"
                    cursor.execute(query)
                return JsonResponse({"message": "Transaksi makan dan daftar pesan berhasil dibuat..."})
        except:
            return JsonResponse({"message": "Sistem sedang gangguan..."})