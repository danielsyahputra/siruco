from django.http.response import JsonResponse
from django.db import connection
from django.db.utils import IntegrityError, InternalError
from database import MyDatabase

def penggunaAPI(request):
    if request.method == "POST":
        try:
            db = MyDatabase()
            username = request.POST['username']
            password = request.POST['password']
            query = f"select * from akun_pengguna where username='{username}' and password='{password}'"
            result = db.query(query=query, fetchOne=True)
            if result:
                query = f"SELECT peran FROM akun_pengguna WHERE username='{username}'"
                peran = db.query(query=query, fetchOne=True)
                peran = peran[0]
                if peran == 'dokter':
                    query = f"SELECT nostr FROM dokter WHERE username='{username}'"
                    nostr = db.query(query=query, fetchOne=True)
                    nostr = nostr[0]
                    request.session['nostr'] = nostr
                    print(username, password, nostr)
                request.session['username'] = username
                request.session['peran'] = peran
                return JsonResponse({"message": "Login berhasil!", "status": 200})
            else:
                query = f"select * from akun_pengguna where username='{username}'"
                result = db.query(query=query, fetchAll=True)
                if result:
                    return JsonResponse({"message": "Password anda salah!", "status": 404})
                else:
                    return JsonResponse({"message": "Akun tidak ditemukan!", "status": 404})
        except:
            return JsonResponse({"message": "Sistem sedang gangguan...", "status": 404})

def kodeFaskes(request):
    db = MyDatabase()
    json = {"kode":[]}
    if request.method == "GET":
        query = "SELECT kode from faskes"
        result = db.query(query=query, fetchAll=True)
        db.close()
        for kode in result:
            json["kode"].append(kode[0])
        return JsonResponse(json, safe=False, json_dumps_params={'indent': 2})
    return JsonResponse({"message": "Sistem sedang gangguan..."})
    
def registerAPI(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        peran = request.POST["role"]
        akun_pengguna = (username, password, peran)
        query_admin = f"INSERT INTO ADMIN VALUES('{username}')"
        with connection.cursor() as cursor:
            try:
                query = f"INSERT INTO AKUN_PENGGUNA VALUES{str(akun_pengguna)}"
                cursor.execute(query)
                if peran == 'admin':
                    cursor.execute(query_admin)
                elif peran == 'admin_satgas':
                    id_faskes = request.POST["kode_faskes"]
                    admin_satgas = (username, id_faskes)
                    query = f"INSERT INTO ADMIN_SATGAS VALUES{str(admin_satgas)}"
                    cursor.execute(query_admin)
                    cursor.execute(query)
                elif peran == 'dokter':
                    nama = request.POST['nama']
                    no_hp = request.POST['no_telp']
                    no_str = request.POST['no_str']
                    gelar_depan = request.POST['gelar_depan']
                    gelar_belakang = request.POST['gelar_belakang']
                    dokter = (username, no_str, nama, no_hp, gelar_depan, gelar_belakang)
                    cursor.execute(query_admin)
                    query = f"INSERT INTO DOKTER VALUES{str(dokter)}"
                    cursor.execute(query)
                else:
                    nama = request.POST['nama']
                    nik = request.POST['nik']
                    no_hp = request.POST['no_telp']
                    status = 'Aktif'
                    pengguna_publik = (username, nik, nama, status, peran, no_hp)
                    query = f"INSERT INTO PENGGUNA_PUBLIK VALUES{str(pengguna_publik)}"
                return JsonResponse({"status": 200, "message": "Akun telah dibuat!"}, safe=False)
            except IntegrityError:
                return JsonResponse({"status": 404, "message": "Username sudah ada!"}, safe=False)
            except InternalError:
                return JsonResponse({"status": 404, "message": "Password harus berisikan minimal satu angka dan huruf kapital!"}, safe=False)
    return JsonResponse({"status": 404, "message": "Sistem sedang gangguan"}, safe=False)