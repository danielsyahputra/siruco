from django.db import connection
from django.http.response import JsonResponse
from django.db.utils import IntegrityError, DataError

def paketMakanAPI(request):
    if request.method == "POST":
        try:
            kodeHotel = request.POST['kodeHotel']
            kodePaket = request.POST['kodePaket']
            namaPaket = request.POST['namaPaket']
            harga = request.POST['harga']
            paketMakan = (kodeHotel, kodePaket, namaPaket, harga)
            with connection.cursor() as cursor:
                query = f"INSERT INTO PAKET_MAKAN VALUES{str(paketMakan)}"
                cursor.execute(query)
                return JsonResponse({"message": f"Paket makan dengan kode {kodePaket} berhasil dibuat..."})
        except IntegrityError:
            return JsonResponse({"message": "Error: Data paket makan sudah pernah dibuat..."})
        except DataError:
            return JsonResponse({"message": "Error: Kode paket melebihi 5 karakter atau Nama paket melebihi 20 karakter."})
        except:
            return JsonResponse({"message": "Error: Sistem sedang gangguan..."})

def paketMakanAPI2(request):
    if request.method == "POST":
        try:
            kodeHotel = request.POST['kodeHotel']
            kodePaket = request.POST['kodePaket']
            namaPaket = request.POST['namaPaket']
            harga = request.POST['harga']
            with connection.cursor() as cursor:
                query = f"""
                UPDATE PAKET_MAKAN
                SET nama='{namaPaket}',
                harga='{harga}'
                WHERE kodehotel='{kodeHotel}' AND
                kodepaket='{kodePaket}'
                """
                cursor.execute(query)
                return JsonResponse({"message": f"Paket makan {kodePaket} pada hotel {kodeHotel} berhasil diperbaharui..."})
        except DataError:
            return JsonResponse({"message": "Error: Kode paket melebihi 5 karakter atau Nama paket melebihi 20 karakter."})
        except:
            return JsonResponse({"message": "Sistem sedang gangguan..."})
    else:
        kodeHotel = request.GET['kodeHotel']
        kodePaket = request.GET['kodePaket']
        with connection.cursor() as cursor:
            query = f"DELETE FROM PAKET_MAKAN WHERE kodehotel='{kodeHotel}' AND kodepaket='{kodePaket}'"
            cursor.execute(query)
            return JsonResponse({"message": f"Paket makan {kodePaket} pada hotel {kodeHotel} berhasil dihapus..."})