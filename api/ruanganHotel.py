from django.db import connection
from django.http.response import JsonResponse

from .helper import generateKodeRuanganHotel

def ruanganHotelAPI(request):
    if request.method == "GET":
        kodeHotel = request.GET['kodeHotel']
        kodeRoom = generateKodeRuanganHotel(kodeHotel)
        return JsonResponse({"kodeRoom": kodeRoom})
    else:
        kodeHotel = request.POST['kodeHotel']
        kodeRoom = request.POST['kodeRuangan']
        jenisBed = request.POST['jenisBed']
        tipe = request.POST['tipe']
        harga = request.POST['harga']
        ruanganHotel = (kodeHotel, kodeRoom, jenisBed, tipe, harga)
        with connection.cursor() as cursor:
            query = f"INSERT INTO HOTEL_ROOM VALUES{str(ruanganHotel)}"
            cursor.execute(query)
            return JsonResponse({"message": f"Ruangan hotel {kodeRoom} pada hotel {kodeHotel} berhasil dibuat..."})

def updateRuanganAPI(request):
    if request.method == "POST":
        kodeHotel = request.POST['kodeHotel']
        kodeRoom = request.POST['kodeRuangan']
        jenisBed = request.POST['jenisBed']
        tipe = request.POST['tipe']
        harga = request.POST['harga']
        with connection.cursor() as cursor:
            query = f"""
            UPDATE HOTEL_ROOM
            SET jenisbed='{jenisBed}',
            tipe='{tipe}',
            harga='{harga}'
            WHERE kodehotel='{kodeHotel}' AND
            koderoom='{kodeRoom}'
            """
            cursor.execute(query)
            return JsonResponse({"message": f"Data pada ruangan hotel {kodeRoom} pada hotel {kodeHotel} berhasil diperbaharui..."})

def deleteRuanganAPI(request):
    try:
        kodeHotel = request.GET['kodeHotel']
        kodeRuangan = request.GET['kodeRuangan']
        with connection.cursor() as cursor:
            query = f"DELETE FROM HOTEL_ROOM WHERE kodehotel='{kodeHotel}' AND koderoom='{kodeRuangan}'"
            cursor.execute(query)
            return JsonResponse({"message": f"Ruangan hotel {kodeRuangan} pada hotel {kodeHotel} berhasil dihapus..."})
    except:
        return JsonResponse({"message": "Sistem sedang gangguan..."})