from django.db import connection
from django.http.response import JsonResponse
from django.db.utils import IntegrityError

from .helper import generateKodeHotel

def hotelAPI(request):
    if request.method == "GET":
        kodeHotel = generateKodeHotel()
        return JsonResponse({"kodeHotel": kodeHotel})
    else:
        try:
            kodeHotel = request.POST['kodeHotel']
            namaHotel = request.POST['nama']
            jalan = request.POST['jalan']
            kelurahan = request.POST['kelurahan']
            kecamatan = request.POST['kecamatan']
            kabkot = request.POST['kabupaten']
            provinsi = request.POST['provinsi']
            rujukan = "1" if "rujukan" in request.POST.keys() else "0"
            hotel = (kodeHotel, namaHotel, rujukan, jalan,
                     kelurahan, kecamatan, kabkot, provinsi
                    )
            with connection.cursor() as cursor:
                query = f"INSERT INTO HOTEL VALUES{str(hotel)}"
                cursor.execute(query)
                return JsonResponse({"message": "Hotel telah berhasil dibuat..."})
        except:
            return JsonResponse({"message": "Sistem sedang gangguan..."})

def updateHotelAPI(request):
    if request.method == "GET":
        kodeHotel = request.GET['kodeHotel']
        with connection.cursor() as cursor:
            query = f"SELECT * FROM HOTEL where kode='{kodeHotel}'"
            cursor.execute(query)
            result = cursor.fetchone()
            return JsonResponse({"data": result}, safe=False)
    else:
        try:
            kodeHotel = request.POST['kodeHotel']
            namaHotel = request.POST['nama']
            jalan = request.POST['jalan']
            kelurahan = request.POST['kelurahan']
            kecamatan = request.POST['kecamatan']
            kabkot = request.POST['kabupaten']
            provinsi = request.POST['provinsi']
            rujukan = "1" if "rujukan" in request.POST.keys() else "0"
            with connection.cursor() as cursor:
                query = f"""
                UPDATE HOTEL
                SET nama='{namaHotel}',
                isrujukan='{rujukan}',
                jalan='{jalan}',
                kelurahan='{kelurahan}',
                kecamatan='{kecamatan}',
                kabkot='{kabkot}',
                provinsi='{provinsi}'
                WHERE kode='{kodeHotel}'
                """
                cursor.execute(query)

                return JsonResponse({
                    "message": f"Data hotel {namaHotel} dengan kode {kodeHotel} berhasil diperbaharui...",
                    "nama": namaHotel,
                    "alamat": f"Jalan {jalan}, Kel.{kelurahan}, Kec.{kecamatan}, {kabkot}, {provinsi}",
                    "rujukan": rujukan})
        except:
            return JsonResponse({"message": "Sistem sedang gangguan..."})