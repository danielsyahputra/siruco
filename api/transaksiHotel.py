from django.db import connection
from django.http.response import JsonResponse

def transaksiHotelAPI(request):
    if request.method == "POST":
        idTransaksi = request.POST['idTransaksi']
        status = request.POST['status']
        tanggal_bayar = request.POST['tanggal_bayar']
        waktu_bayar = request.POST['waktu_bayar']
        with connection.cursor() as cursor:
            query = f"""UPDATE TRANSAKSI_HOTEL 
            SET statusbayar='{status}',
            tanggalpembayaran='{tanggal_bayar}',
            waktupembayaran='{waktu_bayar}'
            WHERE idtransaksi='{idTransaksi}'"""
            cursor.execute(query)
            return JsonResponse({"message": f"Transaksi hotel {idTransaksi} berhasil diperbaharui..."})