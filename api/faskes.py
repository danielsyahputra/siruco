from django.db import connection
from django.http.response import JsonResponse
from django.db.utils import IntegrityError

from .helper import generateKodeFaskes

def faskesAPI(request):
    if request.method == "POST":
        kodeFaskes = request.POST['kodeFaskes']
        shift = request.POST['shift']
        tanggal = request.POST['tanggal']
        jadwalFaskes = (kodeFaskes, shift, tanggal)
        try:
            with connection.cursor() as cursor:
                query = f"INSERT INTO JADWAL VALUES{str(jadwalFaskes)}"
                cursor.execute(query)
                return JsonResponse({"message": "Jadwal faskes telah berhasil dibuat..."})
        except IntegrityError:
            return JsonResponse({"message": "Data yang diisikan sudah terdapat di database, silahkan masukkan pilihan yang lain."})

def buatFaskesAPI(request):
    if request.method == "GET":
        kodeFaskes = generateKodeFaskes()
        return JsonResponse({"kodeFaskes": kodeFaskes})
    else:
        kodeFaskes = request.POST["kodeRS"]
        tipe = request.POST["tipe"]
        nama = request.POST["nama"]
        status = request.POST["status"]
        jalan = request.POST["jalan"]
        kelurahan = request.POST["kelurahan"]
        kecamatan = request.POST["kecamatan"]
        kabupaten = request.POST["kabupaten"]
        provinsi = request.POST["provinsi"]
        faskes = (kodeFaskes, tipe, nama,
                status, jalan, kelurahan,
                kecamatan, kabupaten, provinsi)
        with connection.cursor() as cursor:
            query = f"INSERT INTO FASKES VALUES{str(faskes)}"
            cursor.execute(query)
            return JsonResponse({"message": "Data faskes telah berhasil dibuat..."})

def updateFaskesAPI(request):
    with connection.cursor() as cursor:
        if request.method == "POST":
            kodeFaskes = request.POST["kodeFaskes"]
            tipe = request.POST["tipe"]
            nama = request.POST["nama"]
            status = request.POST["status"]
            jalan = request.POST["jalan"]
            kelurahan = request.POST["kelurahan"]
            kecamatan = request.POST["kecamatan"]
            kabupaten = request.POST["kabupaten"]
            provinsi = request.POST["provinsi"]
            query = f"""
            UPDATE FASKES
            SET tipe='{tipe}',
            nama='{nama}',
            statusmilik='{status}',
            jalan='{jalan}',
            kelurahan='{kelurahan}',
            kecamatan='{kecamatan}',
            kabkot='{kabupaten}',
            prov='{provinsi}'
            WHERE kode='{kodeFaskes}'
            """
            cursor.execute(query)
            return JsonResponse({"message": f"Data faskes {kodeFaskes} telah berhasil diperbaharui"})
        else:
            kodeFaskes = request.GET["kodeFaskes"]
            cursor.execute(f"SELECT * FROM FASKES WHERE kode='{kodeFaskes}'")
            result = cursor.fetchone()
            return JsonResponse({"data": result}, safe=False)

def deleteFaskesAPI(request):
    kodeFaskes = request.GET['kodeFaskes']
    with connection.cursor() as cursor:
        query = f"DELETE FROM FASKES WHERE kode='{kodeFaskes}'"
        cursor.execute(query)
        return JsonResponse({"message": f"Faskes dengan kode {kodeFaskes} berhasil dihapus..."})