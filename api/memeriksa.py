from database import MyDatabase
from django.db import connection
from django.http.response import JsonResponse
from django.db.utils import IntegrityError, InternalError

def appointmentAPI(request):
    peran = request.session['peran']
    if request.method == "POST":
        nik = request.POST['nik']
        nostr = request.POST['nostr']
        dokter = request.POST['dokter']
        faskes = request.POST['kode_faskes']
        shift = request.POST['shift']
        tanggal = request.POST['tanggal']
        try:
            with connection.cursor() as cursor:
                appointement = (nik, nostr, dokter, faskes, shift, tanggal)
                query = f"INSERT INTO MEMERIKSA VALUES{str(appointement)}"
                cursor.execute(query)
                return JsonResponse({"message": "Appointment telah berhasil dibuat"})
        except InternalError:
            return JsonResponse({"message": "Silahkan pilih shift dan tanggal lainnya, karena shift dan tanggal yang dipilih sudah penuh"})
        except IntegrityError:
            return JsonResponse({"message": "Pasien telah terdaftar untuk pilihan jadwal dengan shift dan tanggal tersebut."})
    elif request.method == "GET":
        username = request.session['username']
        db = MyDatabase()
        query = f"SELECT nik, nama FROM PASIEN WHERE idpendaftar='{username}'"
        result = db.query(query=query, fetchAll=True)
        return JsonResponse({"data": result}, safe=False)
    return JsonResponse({"message": "Sistem sedang gangguan, silahkan coba lagi nanti..."}, safe=False)

def appointmentAPI2(request):
    nostr = request.session['nostr']
    with connection.cursor() as cursor:
        if request.method == "POST":
            nik = request.POST['nik']
            dokter = request.POST['dokter']
            faskes = request.POST['kode_faskes']
            shift = request.POST['shift']
            tanggal = request.POST['tanggal']
            rekomendasi = request.POST['rekomendasi']
            query = f"""UPDATE MEMERIKSA SET rekomendasi='{rekomendasi}' WHERE 
            nik_pasien='{nik}' AND 
            nostr='{nostr}' AND 
            username_dokter = '{dokter}' AND
            kode_faskes = '{faskes}' AND
            praktek_shift = '{shift}' AND
            praktek_tgl = '{tanggal}'
            """
            cursor.execute(query)
            return JsonResponse({"message": "Appointment telah berhasil diperbaharui"})
        else:
            nik = request.GET['nik']
            dokter = request.GET['dokter']
            faskes = request.GET['kode_faskes']
            shift = request.GET['shift']
            tanggal = request.GET['tanggal']
            query = f""" 
            DELETE FROM MEMERIKSA WHERE
            nik_pasien='{nik}' AND 
            nostr='{nostr}' AND 
            username_dokter = '{dokter}' AND
            kode_faskes = '{faskes}' AND
            praktek_shift = '{shift}' AND
            praktek_tgl = '{tanggal}'
            """
            cursor.execute(query)
            return JsonResponse({"message": f"Appointment oleh pasien dengan NIK='{nik}' telah berhasil dihapus"})