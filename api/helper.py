import re

from django.db import connection

def generateKodeRuangan(kodeRS):
    kode = int(re.findall(r'\d+', kodeRS)[0]) + 1
    kodeRuangan = "R{:02d}".format(kode)
    return kodeRuangan

def generateKodeBed(kodeRS, kodeRuangan):
    with connection.cursor() as cursor:
        query = f"""SELECT kodebed FROM BED_RS 
        WHERE 
        koderuangan='{kodeRuangan}' AND 
        koders='{kodeRS}' 
        ORDER BY kodebed DESC LIMIT 1
        """
        cursor.execute(query)
        result = cursor.fetchone()[0]
        kode = int(re.findall(r'\d+', result)[0]) + 1
        kodeBed = "B{:02d}".format(kode)
        return kodeBed

def generateKodeFaskes():
    with connection.cursor() as cursor:
        query = "SELECT kode FROM FASKES ORDER BY kode DESC LIMIT 1"
        cursor.execute(query)
        result = cursor.fetchone()[0]
        kode = int(re.findall(r'\d+', result)[0]) + 1
        kodeFaskes = "F{:02d}".format(kode)
        return kodeFaskes

def generateKodeHotel():
    with connection.cursor() as cursor:
        query = "SELECT kode FROM HOTEL ORDER BY kode DESC LIMIT 1"
        cursor.execute(query)
        result = cursor.fetchone()[0]
        kode = int(re.findall(r'\d+', result)[0]) + 1
        kodeHotel = "H{:02d}".format(kode)
        return kodeHotel

def generateKodeRuanganHotel(kodeHotel):
    result = ""
    with connection.cursor() as cursor:
        query = f"SELECT koderoom FROM HOTEL_ROOM WHERE kodehotel='{kodeHotel}' ORDER BY koderoom DESC LIMIT 1"
        cursor.execute(query)
        result = cursor.fetchone()
        if not result:
            return "RH001"
        result = result[0]
        kode = int(re.findall(r'\d+', result)[0]) + 1
        kodeRoom = "RH{:03d}".format(kode)
        return kodeRoom

def generateKodeTRM(idTransaksi):
    result = ""
    with connection.cursor() as cursor:
        query = f"""
        SELECT idtransaksimakan 
        FROM TRANSAKSI_MAKAN 
        WHERE idtransaksi='{idTransaksi}' 
        ORDER BY idtransaksimakan 
        DESC LIMIT 1"""
        cursor.execute(query)
        result = cursor.fetchone()
        if not result:
            return "TRM001"
        result = result[0]
        kode = int(re.findall(r'\d+', result)[0]) + 1
        idTRM = "TRM{:03d}".format(kode)
        return idTRM

def ambilKodeHotel(idTransaksi):
    result = ""
    with connection.cursor() as cursor:
        query = f"""
        SELECT kodeHotel
        FROM reservasi_hotel rh
        NATURAL JOIN transaksi_booking trb
        JOIN transaksi_hotel trh ON trh.idtransaksi=trb.idtransaksibooking
        WHERE trh.idtransaksi='{idTransaksi}';
        """
        cursor.execute(query)
        result = cursor.fetchone()[0]
        return result