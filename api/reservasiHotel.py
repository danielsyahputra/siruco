from django.db import connection
from django.http.response import JsonResponse

from django.db.utils import IntegrityError

def reservasiHotelAPI(request):
    if request.method == "GET":
        kodeHotel = request.GET['kodeHotel']
        with connection.cursor() as cursor:
            query = f"SELECT koderoom FROM HOTEL_ROOM WHERE kodehotel='{kodeHotel}'"
            cursor.execute(query)
            result = cursor.fetchall()
            return JsonResponse({"data": result}, safe=False)
    else:
        try:
            kodePasien = request.POST['nik']
            tglmasuk = request.POST['tanggal_masuk']
            tglkeluar = request.POST['tanggal_keluar']
            kodeHotel = request.POST['kodeHotel']
            kodeRuangan = request.POST['kodeRuangan']
            reservasi = (kodePasien, tglmasuk, tglkeluar, kodeHotel, kodeRuangan)
            with connection.cursor() as cursor:
                query = f"INSERT INTO RESERVASI_HOTEL VALUES{str(reservasi)}"
                cursor.execute(query)
                return JsonResponse({"message": f"Reservasi hotel dari pasien dengan NIK {kodePasien} pada tanggal {tglmasuk} berhasil dibuat..."})
        except IntegrityError:
            return JsonResponse({"message": f"Reservasi hotel pada pasien dengan NIK {kodePasien} pada tanggal {tglmasuk} sudah pernah dibuat..."})
        except:
            return JsonResponse({"message": "Sistem sedang gangguan..."})

def updateReservasiHotelAPI(request):
    if request.method == "POST":
        nik = request.POST['nik']
        tanggal_masuk = request.POST['tanggal_masuk']
        tanggal_keluar = request.POST['tanggal_keluar']
        with connection.cursor() as cursor:
            query = f"""UPDATE RESERVASI_HOTEL 
            SET tglkeluar='{tanggal_keluar}'
            WHERE kodepasien='{nik}' AND
            tglmasuk='{tanggal_masuk}' """
            cursor.execute(query)
            return JsonResponse({"message": f"Reservasi hotel dari pasien dengan NIK {nik} pada tanggal {tanggal_masuk} berhasil diperbaharui..."})

def deleteReservasiHotelAPI(request):
    try:
        nik = request.GET['nik']
        tanggalMasuk = request.GET['tanggalMasuk']
        with connection.cursor() as cursor:
            query = f"DELETE FROM TRANSAKSI_BOOKING WHERE kodepasien='{nik}' AND tglmasuk='{tanggalMasuk}'"
            cursor.execute(query)
            return JsonResponse({"message": f"Reservasi hotel pasien dengan NIK {nik} pada tanggal {tanggalMasuk} berhasil dihapus..."})
    except:
        return JsonResponse({"message": "Sistem sedang gangguan..."})