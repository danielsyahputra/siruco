from django.urls import path

from . import (pasien, pengguna, 
jadwal, memeriksa, 
ruanganRS, faskes, 
rumahSakit, reservasiRS, 
transaksiRS, hotel,
ruanganHotel, reservasiHotel,
transaksiHotel, transaksiMakan,
paketMakan)

app_name = 'api'

urlpatterns = [

     # Pengguna
     path('penggunaAPI/', pengguna.penggunaAPI, name='penggunaAPI'),
     path('kodeFaskes/', pengguna.kodeFaskes, name='kodeFaskes'),
     path('registerAPI/', pengguna.registerAPI, name='registerAPI'),

     # Pasien
     path('pasienAPI/', pasien.pasienAPI, name='pasienAPI'),
     path('deletePasienAPI/', pasien.deletePasienAPI, name='deletePasienAPI'),
     path('editPasienAPI/', pasien.editPasienAPI, name='editPasienAPI'),

     # Jadwal Dokter
     path('jadwalDokterAPI/', jadwal.jadwalDokterAPI, name='jadwalDokterAPI'),

     # Appointment
     path('appointmentAPI/', memeriksa.appointmentAPI, name='appointmentAPI'),
     path('appointmentAPI2/', memeriksa.appointmentAPI2, name='appointmentAPI2'),

     # Ruangan RS
     path('ruanganRSAPI/', ruanganRS.ruanganRSAPI, name='ruanganRSAPI'),
     path('ruanganRSAPI2/', ruanganRS.ruanganRSAPI2, name='ruanganRSAPI2'),

     # Bed RS
     path('bedAPI/', ruanganRS.bedAPI, name='bedAPI'),
     path('kodeBedAPI/', ruanganRS.kodeBedAPI, name='kodeBedAPI'),
     path('deleteBedAPI/', ruanganRS.deleteBedAPI, name='deleteBedAPI'),

     # Faskes
     path('faskesAPI/', faskes.faskesAPI, name='faskesAPI'),
     path('buatFaskesAPI/', faskes.buatFaskesAPI, name='buatFaskesAPI'),
     path('updateFaskesAPI/', faskes.updateFaskesAPI, name='updateFaskesAPI'),
     path('deleteFaskesAPI/', faskes.deleteFaskesAPI, name='deleteFaskesAPI'),

     # Rumah Sakit
     path('rumahSakitAPI/', rumahSakit.rumahSakitAPI, name='rumahSakitAPI'),

     # Reservasi RS
     path('reservasiRSAPI/', reservasiRS.reservasiRSAPI, name='reservasiRSAPI'),
     path('kodeRuanganAPI/', reservasiRS.kodeRuanganAPI, name='kodeRuanganAPI'),
     path('kodeBedAPI2/', reservasiRS.kodeBedAPI, name='kodeBedAPI'),
     path('updateReservasiRSAPI/', reservasiRS.updateReservasiRSAPI, name='updateReservasiRSAPI'),
     path('deleteReservasiAPI/', reservasiRS.deleteReservasiAPI, name='deleteReservasiAPI'),

     # Transaksi RS
     path('transaksiRSAPI/', transaksiRS.transaksiRSAPI, name='transaksiRSAPI'),

     # Hotel
     path('hotelAPI/', hotel.hotelAPI, name='hotelAPI'),
     path('updateHotelAPI/', hotel.updateHotelAPI, name='updateHotelAPI'),

     # Ruangan Hotel
     path('ruanganHotelAPI/', ruanganHotel.ruanganHotelAPI, name='ruanganHotelAPI'),
     path('updateRuanganAPI/', ruanganHotel.updateRuanganAPI, name='updateRuanganAPI'),
     path('deleteRuanganAPI/', ruanganHotel.deleteRuanganAPI, name='deleteRuanganAPI'),

     # Reservasi Hotel
     path('reservasiHotelAPI/', reservasiHotel.reservasiHotelAPI, name='reservasiHotelAPI'),
     path('updateReservasiHotelAPI/', reservasiHotel.updateReservasiHotelAPI,
          name='updateReservasiHotelAPI'),
     path('deleteReservasiHotelAPI/', reservasiHotel.deleteReservasiHotelAPI),

     # Transaksi Hotel
     path('transaksiHotelAPI/', transaksiHotel.transaksiHotelAPI, name='transaksiHotelAPI'),

     # Transaksi Makan
     path('transaksiMakanAPI/', transaksiMakan.transaksiMakanAPI, name='transaksiMakanAPI'),
     
     # Paket Makan
     path('paketMakanAPI/', paketMakan.paketMakanAPI, name='paketMakanAPI'),
     path('paketMakanAPI2/', paketMakan.paketMakanAPI2, name='paketMakanAPI2'),
]
