from django.urls import path

from . import views

app_name = 'transaksiRS'

urlpatterns = [
    path('listTransaksiRS/', views.list_transaksiRS, name='listTransaksiRS')
]
