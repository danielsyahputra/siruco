from django.shortcuts import render
from database import MyDatabase

def list_transaksiRS(request):
    db = MyDatabase()
    context = {}
    query = "SELECT * FROM TRANSAKSI_RS"
    context['transaksi'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'transaksiRS/list_transaksiRS.html', context)