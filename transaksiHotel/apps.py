from django.apps import AppConfig


class TransaksihotelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'transaksiHotel'
