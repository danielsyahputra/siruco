from django.urls import path

from . import views

app_name = 'transaksiHotel'

urlpatterns = [
    path('listTransaksiHotel/', views.listTransaksiHotel, name='listTransaksiHotel'),
    path('listTransaksiBooking/', views.listTransaksiBooking, name='listTransaksiBooking'),
]