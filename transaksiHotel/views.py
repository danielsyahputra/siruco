from django.shortcuts import render

from database import MyDatabase

def listTransaksiHotel(request):
    db = MyDatabase()
    context = {}
    query = "SELECT * FROM TRANSAKSI_HOTEL"
    result = db.query(query=query, fetchAll=True)
    context['datas'] = result
    db.close()
    return render(request, 'transaksiHotel/list_transaksi_hotel.html', context)

def listTransaksiBooking(request):
    db = MyDatabase()
    context = {}
    query = "SELECT * FROM TRANSAKSI_BOOKING"
    result = db.query(query=query, fetchAll=True)
    context['datas'] = result
    db.close()
    return render(request, 'transaksiHotel/list_transaksi_booking.html', context)