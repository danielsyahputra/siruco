from django.urls import path

from . import views

app_name = 'faskes'

urlpatterns = [
    path('listFaskes/', views.list_faskes, name='listFaskes'),
    path('buatFaskes/', views.buat_faskes, name='buatFaskes'),
    path('listJadwalFaskes/', views.list_jadwal_faskes, name='listJadwalFaskes'),
    path('buatJadwalFaskes/', views.buat_jadwal_faskes, name='buatJadwalFaskes')
]