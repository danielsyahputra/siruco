from django.shortcuts import render
from database import MyDatabase

def list_faskes(request):
    db = MyDatabase()
    context = {}
    query = f"SELECT kode, tipe, nama FROM FASKES"
    result = db.query(query=query, fetchAll=True)
    db.close()
    context['faskes'] = result
    return render(request, 'faskes/list_faskes.html', context)

def buat_faskes(request):
    return render(request, 'faskes/buat_faskes.html')

def list_jadwal_faskes(request):
    db = MyDatabase()
    context = {}
    query = f"SELECT * FROM JADWAL"
    result = db.query(query=query, fetchAll=True)
    context['jadwals'] = result
    db.close()
    return render(request, 'faskes/list_jadwalFaskes.html', context)

def buat_jadwal_faskes(request):
    db = MyDatabase()
    context = {}
    query = f"SELECT kode FROM FASKES"
    context['faskes'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'faskes/buat_jadwalFaskes.html', context)