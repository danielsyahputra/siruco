from django.shortcuts import render

from database import MyDatabase

def listReservasiHotel(request):
    db = MyDatabase()
    context = {}
    query = f"SELECT * FROM RESERVASI_HOTEL"
    result = db.query(query=query, fetchAll=True)
    context['datas'] = result
    db.close()
    return render(request, 'reservasiHotel/list_reservasi_hotel.html', context)

def buatReservasiHotel(request):
    db = MyDatabase()
    context = {}
    query = "SELECT nik, nama FROM PASIEN"
    context['niks'] = db.query(query=query, fetchAll=True)
    query = "SELECT kode FROM HOTEL"
    context['kodeHotel'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'reservasiHotel/buat_reservasi_hotel.html', context)