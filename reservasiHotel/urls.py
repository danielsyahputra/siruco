from django.urls import path

from . import views

app_name = 'reservasiHotel'

urlpatterns = [
    path('listReservasiHotel/', views.listReservasiHotel, name='listReservasiHotel'),
    path('buatReservasiHotel/', views.buatReservasiHotel, name='buatReservasiHotel'),
]