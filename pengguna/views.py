from re import T
from database import MyDatabase
from django.shortcuts import redirect, render

def login(request):
    return render(request, 'pengguna/login.html')

def register(request):
    return render(request, 'pengguna/register.html')

def logout(request):
    request.session['username'] = ""
    request.session['peran'] = ""
    return redirect('homepage:homepage')