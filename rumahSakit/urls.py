from django.urls import path

from . import views

app_name = 'rumahSakit'

urlpatterns = [
    path('listRumahSakit/', views.list_rumah_sakit, name='listRumahSakit'),
    path('buatRumahSakit/', views.buat_rumah_sakit, name='buatRumahSakit')
]
