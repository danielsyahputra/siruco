from django.shortcuts import render
from database import MyDatabase

def list_rumah_sakit(request):
    db = MyDatabase()
    context = {}
    query = "SELECT * FROM RUMAH_SAKIT"
    context['hospitals'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'rumahSakit/list_rumah_sakit.html', context)

def buat_rumah_sakit(request):
    db = MyDatabase()
    context = {}
    query = f"SELECT kode FROM FASKES"
    context['faskes'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'rumahSakit/buat_rumah_sakit.html', context)
