"""siruco_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls')),
    path('api/', include('api.urls')),
    path('pengguna/', include('pengguna.urls')),
    path('pasien/', include('pasien.urls')),
    path('jadwalDokter/', include('jadwalDokter.urls')),
    path('memeriksa/', include('memeriksa.urls')),
    path('ruanganRS/', include('ruanganRS.urls')),
    path('reservasiRS/', include('reservasiRS.urls')),
    path('faskes/', include('faskes.urls')),
    path('rumahSakit/', include('rumahSakit.urls')),
    path('transaksiRS/', include('transaksiRS.urls')),
    path('hotel/', include('hotel.urls')),
    path('ruanganHotel/', include('ruanganHotel.urls')),
    path('reservasiHotel/', include('reservasiHotel.urls')),
    path('transaksiHotel/', include('transaksiHotel.urls')),
    path('transaksiMakan/', include('transaksiMakan.urls')),
    path('paketMakan/', include('paketMakan.urls')),
]