from django.urls import path

from . import views

app_name = 'ruanganHotel'

urlpatterns = [
    path('listRuanganHotel/', views.listRuanganHotel, name='listRuanganHotel'),
    path('buatRuanganHotel/', views.buatRuanganHotel, name='buatRuanganHotel'),
]
