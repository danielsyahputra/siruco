from django.shortcuts import render

from database import MyDatabase

def listRuanganHotel(request):
    db = MyDatabase()
    context = {}
    query = "SELECT * FROM HOTEL_ROOM"
    result = db.query(query=query, fetchAll=True)
    context['rooms'] = result
    db.close()
    return render(request, 'ruanganHotel/list_ruangan_hotel.html', context)

def buatRuanganHotel(request):
    db = MyDatabase()
    context = {}
    query = "SELECT kode FROM HOTEL"
    result = db.query(query=query, fetchAll=True)
    context['kodeHotel'] = result
    db.close()
    return render(request, 'ruanganHotel/buat_ruangan_hotel.html', context)