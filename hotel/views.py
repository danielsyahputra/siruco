from django.shortcuts import render
from database import MyDatabase

def list_hotel(request):
    db = MyDatabase()
    context = {}
    query = """ 
    SELECT kode, nama, isrujukan,
    CONCAT('Jalan ', jalan, ', Kel.', kelurahan, ', Kec.', kecamatan, ', ', kabkot, ', ', provinsi)
    FROM HOTEL
    """
    result = db.query(query=query, fetchAll=True)
    context['hotel'] = result
    db.close()
    return render(request, 'hotel/list_hotel.html', context)

def buat_hotel(request):
    return render(request, 'hotel/buat_hotel.html')