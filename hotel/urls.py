from django.urls import path

from . import views

app_name = 'hotel'

urlpatterns = [
    path('listHotel/', views.list_hotel, name='listHotel'),
    path('buatHotel/', views.buat_hotel, name='buatHotel'),
]
