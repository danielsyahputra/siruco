from django.shortcuts import render

description = "SIRUCO adalah Sistem Informasi Rujukan COVID-19."

def homepage(request):
    context = {
        "description": description,
        "active": "teal"
    }
    return render(request, 'homepage/homepage.html', context)