from django.shortcuts import render
from database import MyDatabase
import datetime

def list_reservasiRS(request):
    db = MyDatabase()
    context = {}
    today = datetime.date.today()

    # Data yang ga bisa dihapus
    query = f"SELECT * FROM RESERVASI_RS WHERE tglmasuk <= '{today}'"
    result = db.query(query=query, fetchAll=True)
    context['data1'] = result

    # Data yang bisa dihapus
    query = f"SELECT * FROM RESERVASI_RS WHERE tglmasuk > '{today}'"
    result = db.query(query=query, fetchAll=True)
    context['data2'] = result
    db.close()
    return render(request, 'reservasiRS/list_reservasiRS.html', context)

def buat_reservasiRS(request):
    db = MyDatabase()
    context = {}
    query = "SELECT nik, nama FROM PASIEN"
    context['niks'] = db.query(query=query, fetchAll=True)
    query = "SELECT kode_faskes FROM RUMAH_SAKIT"
    context['kodeRS'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'reservasiRS/buat_reservasiRS.html', context)
