from django.urls import path

from . import views

app_name = 'reservasiRS'

urlpatterns = [
    path('listReservasiRS/', views.list_reservasiRS, name='listReservasiRS'),
    path('buatReservasiRS/', views.buat_reservasiRS, name='buatReservasiRS')
]