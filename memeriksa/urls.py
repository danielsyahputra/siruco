from django.urls import path

from . import views

app_name = 'memeriksa'

urlpatterns = [
    path('list_appointment/', views.list_appointment, name='list_appointment'),
    path('buat_appointment/', views.buat_appointment, name='buat_appointment')
]