from django.shortcuts import render
from database import MyDatabase

def list_appointment(request):
    db = MyDatabase()
    context = {}
    peran = request.session['peran']
    username = request.session['username']
    if peran == 'pengguna_publik':
        query = f"SELECT * FROM memeriksa m JOIN pasien p ON p.nik = m.nik_pasien WHERE p.idpendaftar='{username}'"
        context['appointements'] = db.query(query=query, fetchAll=True)
    elif peran == 'dokter':
        nostr = request.session['nostr']
        query = f"SELECT* FROM memeriksa WHERE username_dokter='{username}' and nostr='{nostr}'"
        context['appointements'] = db.query(query=query, fetchAll=True)
    else:
        query = f"SELECT * FROM memeriksa"
        context['appointements'] = db.query(query=query,fetchAll=True)
    db.close()
    return render(request, 'memeriksa/list_appointement.html', context)

def buat_appointment(request):
    db = MyDatabase()
    context = {}
    query = f"SELECT * FROM JADWAL_DOKTER"
    context['jadwals'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'memeriksa/buat_appointment.html', context)