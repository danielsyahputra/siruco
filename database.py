import psycopg2

database = "dlfbt0t05k5v"
user = "stxyvmepvysycp"
password = "8398dbfc780b53580058fbe7843236bf2fef1ec4b90fea7201066c31020d4ed0"
host = "ec2-52-72-125-94.compute-1.amazonaws.com"
port = '5432'

class MyDatabase():
    def __init__(self):
        """
        Inisiasi MyDatabase objek.

        Args:
        db: nama database yang digunakan (default: depvea11q5mm4n)
        user: username akun yang digunakan untuk connect ke database (default: tusllmktphagnv)
        """
        self.connection = psycopg2.connect(
            database=database, 
            user=user,
            password=password,
            port=port,
            host=host,
            options=f'-c search_path=siruco')
        self.connection.autocommit = True
        self.cur = self.connection.cursor()

    def query(self, query, fetchOne=False,fetchAll=False):
        """
        Method untuk melakukan suatu query SQL.

        Args:
        query: query yang akan dilakukan pada database (string)
        fetched = boolean value. Suatu penanda apakah perlu dilakukan penarikan data atau tidak. (default = True)

        fetched = False untuk operasi update atau insert.
        return value: List of tupples.
        """
        result = []
        self.cur.execute(query)
        if fetchOne:
            result = self.cur.fetchone()
        if fetchAll:
            result = self.cur.fetchall()
        return result

    def close(self):
        """
        Untuk menutup koneksi ke database.
        """
        self.cur.close()
        self.connection.close()