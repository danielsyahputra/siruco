$(document).ready(function () {
  $("select").formSelect();
});

const load = () => {
    $("#loader").empty();
    $("#loader").append(`<div class="preloader-wrapper active">
        <div class="spinner-layer spinner-blue">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>

        <div class="spinner-layer spinner-red">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>

        <div class="spinner-layer spinner-yellow">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>

        <div class="spinner-layer spinner-green">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>`);
}

// CREATE Operation
const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

const buatPaketMakan = form => {
    let APILink = location.origin + "/api/paketMakanAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            M.toast({ html: "Sedang mengirim data...", classes: "rounded" });
            load();
        },
        success: response => {
            $("#loader").empty();
            M.toast({ html: response.message, classes: "rounded" });
            window.location.href = location.origin + "/paketMakan/listPaketMakan/";
        }
    })
}

$("#btn-buat").click(() => {
    let form = $("#paketMakan-form").serialize();
    buatPaketMakan(form);
});

// UPDATE Operation
const editButtons = document.querySelectorAll(".edit-btn");

const isiForm = row => {
    let kodeHotel = row.children[1].innerHTML;
    let kodePaket = row.children[2].innerHTML;
    let namaPaket = row.children[3].innerHTML;
    let harga = row.children[4].innerHTML;
    $("#kodeHotel").val(kodeHotel);
    $("#kodePaket").val(kodePaket);
    $("#namaPaket").val(namaPaket);
    $("#harga").val(harga);
}

const updatePaketMakan = row => {
    let kodeHotel = row.children[1].innerHTML;
    let kodePaket = row.children[2].innerHTML;
    let namaPaket = $("#namaPaket").val();
    let harga = $("#harga").val();
    const form = {
        "kodeHotel": kodeHotel,
        "kodePaket": kodePaket,
        "namaPaket": namaPaket,
        "harga": harga
    }
    let APILink = location.origin + "/api/paketMakanAPI2/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            row.children[3].innerHTML = namaPaket;
            row.children[4].innerHTML = harga;
        }
    })
}

editButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        isiForm(row);
        $("#kirim-btn").click(() => {
            updatePaketMakan(row);
        });
    })
})

// DELETE Operation

const deleteButtons = document.querySelectorAll(".delete-btn");
const tableBody = document.querySelector("#table-body");

deleteButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        let kodeHotel = row.children[1].innerHTML;
        let kodePaket = row.children[2].innerHTML;
        let APILink = location.origin + "/api/paketMakanAPI2/";
        $.ajax({
            url: APILink,
            type: "GET",
            data: { "kodeHotel": kodeHotel, "kodePaket": kodePaket },
            beforeSend: () => {
                $("#message").html("Sedang menghapus data...");
                tableBody.removeChild(row);
            },
            success: response => {
                $("#message").html(response.message);
            }
        })
    })
})