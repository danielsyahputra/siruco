const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

$(document).ready(function () {
  $("select").formSelect();
  $(".datepicker").datepicker({ format: "yyyy-mm-dd" });
});

$("#kodeHotel-select").change(() => {
    const kodeHotel = $("#kodeHotel-select").val();
    let APILink = location.origin + "/api/reservasiHotelAPI/";
    $.ajax({
        url: APILink,
        type: "GET",
        data: { "kodeHotel": kodeHotel },
        beforeSend: () => {
            $("#kodeRuangan-select").empty();
            $("#kodeRuangan-select").append(
              `<option value="" disabled selected>Sedang mengambil data...</option>`
            );
            $("select").formSelect();
        },
        success: response => {
            let data = response.data;
            $("#kodeRuangan-select").empty();
            if (data.length == 0) {
                $("#kodeRuangan-select").append(
                    `<option value="" disabled selected>Belum ada ruangan...</option>`
                );
            } else {
                $("#kodeRuangan-select").append(
                    `<option value="" disabled selected>Pilih...</option>`
                );
                data.forEach(kode => {
                    $("#kodeRuangan-select").append(
                        `<option value="${kode[0]}">${kode[0]}</option>`
                    );
                })
            }
            $("select").formSelect();
        }
    })
});


const buatReservasiHotel = form => {
    let APILink = location.origin + "/api/reservasiHotelAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            window.location.href =
              location.origin + "/reservasiHotel/listReservasiHotel/";
        }
    })
}

$("#btn-buat").click(() => {
    let form = $("#reservasiHotel-form").serialize();
    buatReservasiHotel(form);
});


// UPDATE Operation
const editButtons = document.querySelectorAll(".edit-btn");

const isiForm = row => {
    const nik = row.children[1].innerHTML;
    const tanggalMasuk = row.children[2].innerHTML;
    const tanggalKeluar = row.children[3].innerHTML;
    const kodeHotel = row.children[4].innerHTML;
    const kodeRuangan = row.children[5].innerHTML;
    $("#nik").val(nik);
    $("#kodeHotel").val(kodeHotel);
    $("#kodeRuangan").val(kodeRuangan);
    $("#tanggal_masuk").val(tanggalMasuk);
    $("#tanggal_keluar").val(tanggalKeluar);
}

const updateReservasiHotel = row => {
    const nik = $("#nik").val();
    const tanggalMasuk = $("#tanggal_masuk").val();
    const tanggalKeluar = $("#tanggal_keluar").val();
    let APILink = location.origin + "/api/updateReservasiHotelAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: { "nik": nik, "tanggal_masuk": tanggalMasuk, "tanggal_keluar": tanggalKeluar },
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            row.children[3].innerHTML = tanggalKeluar;
        }
    })
}

editButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        isiForm(row);
        $("#kirim-btn").click(() => {
            updateReservasiHotel(row);
        });
    })
})

// DELETE Operation
const deleteButtons = document.querySelectorAll(".delete-btn");
const tableBody = document.querySelector("#table-body");

deleteButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        let APILink = location.origin + "/api/deleteReservasiHotelAPI/";
        let nik = row.children[1].innerHTML;
        let tanggalMasuk = row.children[2].innerHTML;
        $.ajax({
            url: APILink,
            type: "GET",
            data: { "nik": nik, "tanggalMasuk": tanggalMasuk },
            beforeSend: () => {
                $("#message").html("Sedang menghapus data...");
                tableBody.removeChild(row);
            },
            success: response => {
                $("#message").html(response.message);
            }
        })
    })
})