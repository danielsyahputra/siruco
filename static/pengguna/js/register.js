$(document).ready(function () {
    $("select").formSelect();
    $(".slider").slider();
    $("input#username, input#password").characterCounter();
});

const isiDropdown = () => {
    const APILink = location.origin + "/api/kodeFaskes/";
    $.ajax({
        url: APILink,
        success: (response) => {
            const kodes = response.kode;
            kodes.forEach(kode => {
                const option = `<option value="${kode}">${kode}</option>`;
                $("#kode_faskes").append(option);
            })
            $("select").formSelect();
        }
    });
}

$("#role-select").change((e) => {
    e.preventDefault();
    const role = $('select[name="role"]').val();
    switch (role) {
        case "pengguna_publik":
            $("#role-form").html(pengguna_publik);
            break;
        case "admin":
            $("#role-form").html(admin);
            break;
        case "admin_satgas":
            $("#role-form").html(admin_satgas);
            isiDropdown();
            break;
        case "dokter":
            $("#role-form").html(dokter);
            break;
        default:
            break;
    }
});

const pengguna_publik = `
<h6 class="black-text text-black">Masukkan data diri anda.</h6>
<div class="row">
    <div class="input-field col s12">
        <i class="material-icons prefix">account_circle</i>
        <input name="username" id="username" type="text" class="validate inputan" data-length="50">
        <label for="first_name">Username</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <i class="material-icons prefix">vpn_key</i>
        <input id="password" name="password" type="password"
            class="validate inputan" data-length="20">
        <label for="password">Password</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
        <i class="material-icons prefix">account_circle</i>
        <input name="nama" id="nama" type="text" class="validate">
        <label for="icon_prefix">Nama</label>
    </div>
    <div class="input-field col s6">
        <i class="material-icons prefix">phone</i>
        <input id="no_telp" name="no_telp" type="tel" class="validate">
        <label for="icon_telephone">Telephone</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <i class="material-icons prefix">assignment_ind</i>
        <input name="nik" id="nik" type="text" class="validate">
        <label for="icon_prefix">NIK</label>
    </div>
</div>
`;


const admin = `
<h6 class="black-text text-black">Masukkan data diri anda.</h6>
<div class="row">
    <div class="input-field col s12">
        <i class="material-icons prefix">account_circle</i>
        <input name="username" id="username" type="text" class="validate inputan" data-length="50">
        <label for="first_name">Username</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <i class="material-icons prefix">vpn_key</i>
        <input id="password" name="password" type="password"
            class="validate inputan" data-length="20">
        <label for="password">Password</label>
    </div>
</div>
`;

const admin_satgas = `
<h6 class="black-text text-black">Masukkan data diri anda.</h6>
<div class="row">
    <div class="input-field col s12">
        <i class="material-icons prefix">account_circle</i>
        <input name="username" id="username" type="text" class="validate inputan" data-length="50">
        <label for="first_name">Username</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <i class="material-icons prefix">vpn_key</i>
        <input id="password" name="password" type="password"
            class="validate inputan" data-length="20">
        <label for="password">Password</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <i class="material-icons prefix">code</i>
        <select name="kode_faskes" id="kode_faskes">
            <option value="" disabled selected>Pilih...</option>
        </select>
        <label>Kode Faskes (Admin Satgas)</label>
    </div>
</div>
`;

const dokter = `
<h6 class="black-text text-black">Masukkan data diri anda.</h6>
<div class="row">
    <div class="input-field col s12">
        <i class="material-icons prefix">account_circle</i>
        <input name="username" id="username" type="text" class="validate inputan" data-length="50">
        <label for="first_name">Username</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <i class="material-icons prefix">vpn_key</i>
        <input id="password" name="password" type="password"
            class="validate inputan" data-length="20">
        <label for="password">Password</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
        <i class="material-icons prefix">account_circle</i>
        <input name="nama" id="nama" type="text" class="validate">
        <label for="icon_prefix">Nama</label>
    </div>
    <div class="input-field col s6">
        <i class="material-icons prefix">phone</i>
        <input id="no_telp" name="no_telp" type="tel" class="validate">
        <label for="icon_telephone">Telephone</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <i class="material-icons prefix">assignment_ind</i>
        <input name="no_str" id="no_str" type="text" class="validate">
        <label for="icon_prefix">No.STR</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
        <i class="material-icons prefix">account_circle</i>
        <input name="gelar_depan" id="gelar_depan" type="text" class="validate">
        <label for="icon_prefix">Gelar Depan</label>
    </div>
    <div class="input-field col s6">
        <i class="material-icons prefix">account_circle</i>
        <input id="gelar_belakang" name="gelar_belakang" type="tel" class="validate">
        <label for="icon_telephone">Gelar Belakang</label>
    </div>
</div>
`;