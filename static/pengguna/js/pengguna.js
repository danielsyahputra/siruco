const load = () => {
    $("#message").empty();
    $("#loader").empty();
    $("#loader").append(`<div class="preloader-wrapper active">
        <div class="spinner-layer spinner-blue">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>

        <div class="spinner-layer spinner-red">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>

        <div class="spinner-layer spinner-yellow">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>

        <div class="spinner-layer spinner-green">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>`);
};

// LOGIN
const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

const login = form => {
    const APILink = location.origin + "/api/penggunaAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: function () {
            M.toast({ html: "Sedang mengirim data...", classes: "rounded" });
            load();
        },
        success: function (response) {
            $("#loader").empty();
            M.toast({ html: response.message, classes: "rounded" });
            if (response.status == 200) window.location.href = location.origin;
        },
    });
};
$("#btn-login").click(() => {
    login($("#signup-form").serialize());
});

// REGISTER
const signup = (form) => {
    const APILink = location.origin + "/api/registerAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            M.toast({ html: "Sedang mengirim data...", classes: "rounded" });
            load();
        },
        success: (data) => {
            const status = data.status;
            const message = data.message;
            $("#loader").empty();
            M.toast({ html: message, classes: "rounded" });
            if (status === 200) {
                window.location.href = location.origin + "/pengguna/login/";
            }
        },
    });
};
$("#btn-register").click(() => {
    signup($("#signup-form").serialize());
});