$(document).ready(function () {
  $("select").formSelect();
});

const isiKodePaket = data => {
    $("#kodePaket-select").empty();
    const dataLength = data.length;
    if (dataLength === 0) {
        $("#kodePaket-select").append(
          `<option value="" disabled selected>Belum ada paket makan...</option>`
        );
    } else {
        $("#kodePaket-select").append(
          `<option value="" disabled>Pilih...</option>`
        );
        data.forEach(kode => {
            $("#kodePaket-select").append(
              `<option value="${kode[0]}">${kode[0]}</option>`
            );
        })
    }
    $("select").formSelect();
}

$("#idt-select").change(() => {
    let idTransaksi = $("#idt-select").val();
    let APILink = location.origin + "/api/transaksiMakanAPI/";
    $.ajax({
        url: APILink,
        type: "GET",
        data: {"idt": idTransaksi},
        beforeSend: () => {
            $("#idtm").val("");
            $("#kodeHotel").val("");
            $("#idtm").attr("placeholder", "Sedang proses...");
            $("#kodeHotel").attr("placeholder", "Sedang proses...");
            $("#kodePaket-select").empty();
            $("#kodePaket-select").append(
              `<option value="" disabled selected>Sedang mengambil data...</option>`
            );
            $("select").formSelect();
        },
        success: response => {
            const idTRM = response.idTRM;
            const kodeHotel = response.kodeHotel;
            const data = response.data;
            $("#idtm").val(idTRM);
            $("#kodeHotel").val(kodeHotel);
            isiKodePaket(data);
        }
    })
});


// CREATE Operation
const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

const ambilDataForm = () => {
    let data = {}
    let idTransaksi = $("#idt-select").val();
    let idTRM = $("#idtm").val();
    let kodeHotel = $("#kodeHotel").val();
    let kodePaket = $("#kodePaket-select").val().join("_");
    data["idt"] = idTransaksi;
    data["idTRM"] = idTRM;
    data["kodeHotel"] = kodeHotel;
    data["kodePaket"] = kodePaket;
    return data;
}

const buatTransaksiMakan = form => {
    let APILink = location.origin + "/api/transaksiMakanAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            window.location.href =
              location.origin + "/transaksiMakan/listTransaksiMakan/";
        }
    })
}

$("#btn-buat").click(() => {
    let form = ambilDataForm();
    buatTransaksiMakan(form);
});