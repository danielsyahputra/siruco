const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

$(document).ready(() => {
    let APILink = location.origin + "/api/hotelAPI/";
    $.ajax({
        url: APILink,
        type: "GET",
        beforeSend: () => {
            $("#kodeHotel").attr("placeholder", "Sedang mengambil data...");
        },
        success: response => {
            const kodeHotel = response.kodeHotel;
            $("#kodeHotel").val(kodeHotel);
        }
    })
})


// CREATE Operation
const buatHotel = form => {
    let APILink = location.origin + "/api/hotelAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            window.location.href = location.origin + "/hotel/listHotel/";
        }
    });
}

$("#btn-buat").click(() => {
    let form = $("#hotel-form").serialize();
    buatHotel(form);
});


// UPDATE Operation
const editButtons = document.querySelectorAll(".edit-btn");

const isiForm = kodeHotel => {
    let APILink = location.origin + "/api/updateHotelAPI/";
    $.ajax({
        url: APILink,
        type: "GET",
        data: { "kodeHotel": kodeHotel },
        beforeSend: () => {
            $("#message-modal").html("Sedang mengambil data...");
        },
        success: response => {
            const data = response.data;
            $("#message-modal").html(
              "Silahkan masukkan data yang ingin diperbaharui..."
            );
            $("#kodeHotel").val(data[0]);
            $("#nama").val(data[1]);
            $("#rujukan").val(data[2]);
            $("#jalan").val(data[3]);
            $("#kelurahan").val(data[4]);
            $("#kecamatan").val(data[5]);
            $("#kabupaten").val(data[6]);
            $("#provinsi").val(data[7]);
        }
    })
}

const updateHotel = (form, row) => {
    let APILink = location.origin + "/api/updateHotelAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            row.children[2].innerHTML = response.nama;
            row.children[4].innerHTML = response.alamat;
            if (rujukan == "1") {
                row.children[3].innerHTML = `<i class="material-icons">check</i>`;
            } else {
                row.children[3].innerHTML = `<i class="material-icons">close</i>`;
            }
        }
    })
}

editButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        let kodeHotel = row.children[1].innerHTML;
        $("#kodeHotel").val(kodeHotel);
        isiForm(kodeHotel);
        $("#kirim-btn").click(() => {
            let form = $("#hotel-form").serialize();
            updateHotel(form, row);
        });
    })
})