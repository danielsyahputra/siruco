const inputFields = document.querySelectorAll(".input-field");

const checkForm = () => {
    let isValid = true;
    inputFields.forEach(inputField => {
        let inputValue = inputField.children[1].value;
        if (inputValue === "") isValid = false;
    })
    return isValid;
}

inputFields.forEach(inputField => {
    let input = inputField.children[1];
    inputField.addEventListener('change', () => {
        if (checkForm()) {
            $("#message").css("color", "#00796b");
            $("#message").html("Data yang diisikan sudah lengkap");
        } else {
            $("#message").css("color", "#ff5252");
            $("#message").html("Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu");
        }
    })
})