$(document).ready(function () {
  $("select").formSelect();
});

$(document).ready(function () {
  $(".datepicker").datepicker({ format: "yyyy-mm-dd" });
});

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

const buatJadwalFaskes = form => {
    let APILink = location.origin + "/api/faskesAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            window.location.href =
                location.origin + "/faskes/listJadwalFaskes/";
        }
    });
};

$("#btn-buat").click(() => {
    buatJadwalFaskes($("#jadwalFaskes-form").serialize());
});