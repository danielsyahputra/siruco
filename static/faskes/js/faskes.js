$(document).ready(function () {
  $("select").formSelect();
});

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

$(document).ready(() => {
    let APILink = location.origin + "/api/buatFaskesAPI/";
    $.ajax({
        url: APILink,
        type: "GET",
        beforeSend: () => {
            $("#kodeRS").attr("placeholder", "Sedang mengambil data");
        },
        success: response => {
            const kodeFaskes = response.kodeFaskes;
            $("#kodeRS").attr("placeholder", kodeFaskes);
            $("#kodeRS").val(kodeFaskes);
        }
    })
});

const buatFaskes = form => {
    let APILink = location.origin + "/api/buatFaskesAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            window.location.href = location.origin + "/faskes/listFaskes/";
        }
    });
}

$("#btn-buat").click(() => {
    let form = $("#faskes-form").serialize();
    buatFaskes(form);
});

// Update Operation
const editButtons = document.querySelectorAll(".edit-btn");

const isiForm = kodeFaskes => {
    let APILink = location.origin + "/api/updateFaskesAPI/";
    $.ajax({
        url: APILink,
        type: "GET",
        data: { "kodeFaskes": kodeFaskes },
        beforeSend: () => {
            $("#message-modal").html("Sedang mengambil data...");
        },
        success: response => {
            const data = response.data;
            $("#message-modal").html("Silahkan masukkan data yang ingin diperbaharui...");
            $("#tipe-select").val(data[1]);
            $("#nama").val(data[2]);
            $("#status-select").val(data[3]);
            $("#jalan").val(data[4]);
            $("#kelurahan").val(data[5]);
            $("#kecamatan").val(data[6]);
            $("#kabupaten").val(data[7]);
            $("#provinsi").val(data[8]);
            $("select").formSelect();
        }
    })
}

const updateFaskes = (form, row) => {
    let APILink = location.origin + "/api/updateFaskesAPI/";
    const tipe = $("#tipe-select").val();
    const nama = $("#nama").val();
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            row.children[2].innerHTML = tipe;
            row.children[3].innerHTML = nama;
        }
    });
};

editButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        let kodeFaskes = row.children[1].innerHTML;
        $("#kodeFaskes").attr("placeholder", kodeFaskes);
        $("#kodeFaskes").val(kodeFaskes);
        let APILink = location.origin + "/api/updateFaskesAPI/";
        isiForm(kodeFaskes);
        $("#kirim-btn").click(() => {
            let form = $("#faskes-form").serialize();
            updateFaskes(form, row);
        });
    })
})

// DELETE Operation
const deleteButtons = document.querySelectorAll(".delete-btn");
const tableBody = document.querySelector("#table-body");

deleteButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        let kodeFaskes = row.children[1].innerHTML;
        let APILink = location.origin + "/api/deleteFaskesAPI/";
        $.ajax({
            url: APILink,
            type: "GET",
            data: { "kodeFaskes": kodeFaskes },
            beforeSend: () => {
                $("#message").html("Sedang menghapus data...");
                tableBody.removeChild(row);
            },
            success: response => {
                $("#message").html(response.message);
            }
        })
    })
})