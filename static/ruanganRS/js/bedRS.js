$(document).ready(function () {
  $("select").formSelect();
});

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

// CREATE Operation

$("#kodeRS-select").change(() => {
    const kodeRS = $("#kodeRS-select").val();
    let APILink = location.origin + "/api/bedAPI/";
    $.ajax({
        url: APILink,
        type: "GET",
        data: { "kodeRS": kodeRS },
        beforeSend: () => {
            $("#kodeRuangan-select").empty();
            $("select").formSelect();
        },
        success: response => {
            const kodeRuang = response.kodeRuangan;
            const banyakRuangan = kodeRuang.length;
            if (banyakRuangan === 0) {
                $("#kodeRuangan-select").append(
                    `<option value="" disabled selected>Belum ada ruangan...</option>`
                );
                $("select").formSelect();
            } else {
                $("#kodeRuangan-select").append(
                    `<option value="" disabled selected>Pilih...</option>`
                );
                kodeRuang.forEach(kode => {
                    $("#kodeRuangan-select").append(
                        `<option value="${kode[0]}">${kode[0]}</option>`
                    );
                });
                $("select").formSelect();
            }
        }
    })
});

$("#kodeRuangan-select").change(() => {
    const kodeRuangan = $("#kodeRuangan-select").val();
    const kodeRS = $("#kodeRS-select").val();
    let APILink = location.origin + "/api/kodeBedAPI/";
    $.ajax({
        url: APILink,
        type: "GET",
        data: {
            "kodeRS": kodeRS,
            "kodeRuangan": kodeRuangan
        },
        success: response => {
            const kodeBed = response.kodeBed;
            $("#kodeBed").attr("placeholder", kodeBed);
            $("#kodeBed").val(kodeBed);
        }
    })
});


$("#btn-buat").click(() => {
    const kodeRuangan = $("#kodeRuangan-select").val();
    const kodeRS = $("#kodeRS-select").val();
    const kodeBed = $("#kodeBed").val();
    let form = {
        "kodeRS": kodeRS,
        "kodeRuangan": kodeRuangan,
        "kodeBed": kodeBed
    }
    let APILink = location.origin + "/api/bedAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            window.location.href = location.origin + "/ruanganRS/list_bedRS/";
        }
    });
});

// Delete Operation
const deleteButtons = document.querySelectorAll(".delete-btn");
const tableBody = document.querySelector("#table-body");

deleteButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        const kodeRuangan = row.children[1].innerHTML;
        const kodeRS = row.children[2].innerHTML;
        const kodeBed = row.children[3].innerHTML;
        let APILink = location.origin + "/api/deleteBedAPI/";
        $.ajax({
            url: APILink,
            type: "GET",
            data: {
                "kodeRS": kodeRS,
                "kodeRuangan": kodeRuangan,
                "kodeBed": kodeBed
            },
            beforeSend: () => {
                $("#message").html("Sedang menghapus data...");
                tableBody.removeChild(row);
            },
            success: response => {
                $("#message").html(response.message);
            }
        })
    })
})