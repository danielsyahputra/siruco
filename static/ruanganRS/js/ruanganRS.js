$(document).ready(function () {
	$("select").formSelect();
});

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

// CREATE Ruangan RS
const buatRuanganRS = () => {
	let APILink = location.origin + "/api/ruanganRSAPI/";
	let form = {
		"kodeRS": $("#kodeRS-select").val(),
		"kodeRuangan": $("#kodeRuangan").val(),
		"tipe": $("#tipe").val(),
		"harga": $("#harga").val()
	};
	$.ajax({
		url: APILink,
		type: "POST",
		headers: { "X-CSRFToken": csrftoken },
		data: form,
		beforeSend: () => {
			$("#message").html("Sedang mengirim data...");
		},
		success: response => {
			$("#message").html(response.message);
			window.location.href = location.origin + "/ruanganRS/list_ruanganRS/";
		}
	});
}

$("#btn-buat").click(() => {
	buatRuanganRS();
});


$("#kodeRS-select").change(() => {
	const kodeRS = $("#kodeRS-select").val();
	let APILink = location.origin + "/api/ruanganRSAPI/";
	$.ajax({
		url: APILink,
		type: "GET",
		data: { "kodeRS": kodeRS },
		success: response => {
			$("#kodeRuangan").val(response.kodeRuangan);
		}
	})
});


// Update Operation
const editButtons = document.querySelectorAll(".edit-btn");

editButtons.forEach(btn => {
	btn.addEventListener('click', () => {
		let row = btn.parentElement.parentElement;
		const kodeRS = row.children[1].innerHTML;
		const kodeRuangan = row.children[2].innerHTML;
		$("#kodeRS").attr("placeholder", kodeRS);
		$("#kodeRuangan").attr("placeholder", kodeRuangan);
		$("#kirim-btn").click(() => {
			const tipe = $("#tipe").val();
			const harga = $("#harga").val();
			let APILink = location.origin + "/api/ruanganRSAPI2/";
			let form = {
				"kodeRS": kodeRS,
				"kodeRuangan": kodeRuangan,
				"tipe": tipe,
				"harga": harga
			}
			$.ajax({
				url: APILink,
				type: "POST",
				headers: { "X-CSRFToken": csrftoken },
				data: form,
				beforeSend: () => {
					$("#message").html("Sedang mengirim data...");
				},
				success: response => {
					$("#message").html(response.message);
					row.children[3].innerHTML = tipe;
					row.children[5].innerHTML = harga;
				}
			});
		});
	})
})