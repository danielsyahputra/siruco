$(document).ready(function () {
    $(".modal").modal();
});

/**
 * LOADER
 */
const load = () => {
    $("#message").empty();
    $("#loader").empty();
    $("#loader").append(`<div class="preloader-wrapper active">
        <div class="spinner-layer spinner-blue">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>

        <div class="spinner-layer spinner-red">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>

        <div class="spinner-layer spinner-yellow">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>

        <div class="spinner-layer spinner-green">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>`);
};

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;
const deleteButtons = document.querySelectorAll(".delete-btn");
const detailButtons = document.querySelectorAll(".detail-btn");
const tableBody = document.querySelector("#table-body");

/**
 * DELETE Operation
 */
deleteButtons.forEach((btn) => {
    const APILink = location.origin + "/api/deletePasienAPI/";
    btn.addEventListener("click", () => {
        let row = btn.parentElement.parentElement;
        let nik = row.children[1].innerHTML;
        $.ajax({
            url: APILink,
            type: "GET",
            data: { "nik": nik },
            beforeSend: function () {
                M.toast({ html: "Sedang menghapus data...", classes: "rounded" });
                load();
                tableBody.removeChild(row);
            },
            success: (response) => {
                $("#loader").empty();
                M.toast({ html: response.message, classes: "rounded" });
            },
        });
    });
});

/**
 * DETAIL Operation
 */
const generateHTML = (dataPasien) => {
    const HTMLText = `
    <div class="row">
        <div class="col s4">
            <h6>Informasi UMUM</h6>
            <p><span style="color: #00796b ;">NIK</span>: ${dataPasien[0]}</p>
            <p><span style="color: #00796b ;">Id Pendaftar</span>: ${dataPasien[1]}</p>
            <p><span style="color: #00796b ;">Nama Pasien</span>: ${dataPasien[2]}</p>
            <p><span style="color: #00796b ;">No Telp</span>: ${dataPasien[13]}</p>
            <p><span style="color: #00796b ;">No HP</span>: ${dataPasien[14]}</p>
        </div>
        <div class="col s4">
            <h6>Alamat (KTP)</h6>
            <p><span style="color: #00796b ;">Jalan</span>: ${dataPasien[3]}</p>
            <p><span style="color: #00796b ;">Kelurahan</span>: ${dataPasien[4]}</p>
            <p><span style="color: #00796b ;">Kecamatan</span>: ${dataPasien[5]}</p>
            <p><span style="color: #00796b ;">Kab/Kota</span>: ${dataPasien[6]}</p>
            <p><span style="color: #00796b ;">Provinsi</span>: ${dataPasien[7]}</p>
        </div>
        <div class="col s4">
            <h6>Alamat (Domisili)</h6>
            <p><span style="color: #00796b ;">Jalan</span>: ${dataPasien[8]}</p>
            <p><span style="color: #00796b ;">Kelurahan</span>: ${dataPasien[9]}</p>
            <p><span style="color: #00796b ;">Kecamatan</span>: ${dataPasien[10]}</p>
            <p><span style="color: #00796b ;">Kab/Kota</span>: ${dataPasien[11]}</p>
            <p><span style="color: #00796b ;">Provinsi</span>: ${dataPasien[12]}</p>
        </div>
    </div>`;
    return HTMLText;
};

detailButtons.forEach((btn) => {
    const APILink = location.origin + "/api/pasienAPI/";
    btn.addEventListener("click", () => {
        let row = btn.parentElement.parentElement;
        let nik = row.children[1].innerHTML;
        $("#detail-header").html("Detail Pasien");
        $.ajax({
            url: APILink,
            type: "GET",
            data: { nik: nik },
            beforeSend: function () {
                M.toast({ html: "Sedang mengambil data...", classes: "rounded" });
                $("#detail-text").empty();
                $("#loader-modal").addClass("progress");
            },
            success: (response) => {
                const dataPasien = response.data;
                $("#detail-text").html(generateHTML(dataPasien));
                M.toast({ html: "Data telah didapatkan...", classes: "rounded" });
                $("#loader-modal").removeClass("progress");
            },
        });
    });
});


/**
 * CREATE Operation
 */

const daftarPasien = (form) => {
    const APILink = location.origin + "/api/pasienAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: function () {
            M.toast({ html: "Sedang mengirim data...", classes: "rounded" });
            load();
        },
        success: function (response) {
            const status = response.status;
            const message = response.message;
            $("#loader").empty();
            M.toast({ html: message, classes: "rounded" });
            if (status === 200)
                window.location.href = location.origin + "/pasien/list_pasien/";
        },
    });
};
$("#btn-daftar").click(() => {
    daftarPasien($("#pasien-form").serialize());
});

/**
 * UPDATE Operation
 */
const edit = (form) => {
    const APILink = location.origin + "/api/editPasienAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            M.toast({ html: "Data sedang di proses...", classes: "rounded" });
            load();
        },
        success: (response) => {
            $("#loader").empty();
            M.toast({ html: response.message, classes: "rounded" });
            window.location.href = location.origin + "/pasien/list_pasien/";
        },
    });
};

$("#btn-edit").click(() => {
    edit($("#pasien-form").serialize());
});