const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;


$("#kodeHotel-select").change(() => {
    const kodeHotel = $("#kodeHotel-select").val();
    let APILink = location.origin + "/api/ruanganHotelAPI/";
    $.ajax({
        url: APILink,
        type: "GET",
        data: { "kodeHotel": kodeHotel },
        beforeSend: () => {
            $("#kodeRuangan").val("");
            $("#kodeRuangan").attr("placeholder", "Sedang mengambil data...");
        },
        success: response => {
            const kodeRoom = response.kodeRoom;
            $("#kodeRuangan").attr("placeholder", kodeRoom);
            $("#kodeRuangan").val(kodeRoom);
        }
    })
});

const buatRuanganHotel = form => {
    let APILink = location.origin + "/api/ruanganHotelAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            window.location.href =
              location.origin + "/ruanganHotel/listRuanganHotel/";
        }
    })
}

$("#btn-buat").click(() => {
    $("#kodeRuangan").attr("disabled", false);
    let form = $("#ruanganHotel-form").serialize();
    $("#kodeRuangan").attr("disabled", true);
    buatRuanganHotel(form);
});


// UPDATE Operation

const editButtons = document.querySelectorAll(".edit-btn");

const isiForm = row => {
    let kodeHotel = row.children[1].innerHTML;
    let kodeRuangan = row.children[2].innerHTML;
    let jenisBed = row.children[3].innerHTML;
    let tipe = row.children[4].innerHTML;
    let harga = row.children[5].innerHTML;
    $("#kodeHotel").val(kodeHotel);
    $("#kodeRuangan").val(kodeRuangan);
    $("#jenisBed").val(jenisBed);
    $("#tipe").val(tipe);
    $("#harga").val(harga);
}

const updateRuanganHotel = (form, row) => {
    let jenisBed = $("#jenisBed").val();
    let tipe = $("#tipe").val();
    let harga = $("#harga").val();
    let APILink = location.origin + "/api/updateRuanganAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            row.children[3].innerHTML = jenisBed;
            row.children[4].innerHTML = tipe;
            row.children[5].innerHTML = harga;
        }
    })
}

editButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        isiForm(row);
        $("#kirim-btn").click(() => {
            $("#kodeHotel").attr("disabled", false);
            $("#kodeRuangan").attr("disabled", false);
            let form = $("#ruanganHotel-form").serialize();
            $("#kodeHotel").attr("disabled", false);
            $("#kodeRuangan").attr("disabled", false);
            updateRuanganHotel(form, row);
        });
    })
})

// DELETE Operation
const deleteButtons = document.querySelectorAll(".delete-btn");
const tableBody = document.querySelector("#table-body");

deleteButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let APILink = location.origin + "/api/deleteRuanganAPI/";
        let row = btn.parentElement.parentElement;
        let kodeHotel = row.children[1].innerHTML;
        let kodeRuangan = row.children[2].innerHTML;
        $.ajax({
            url: APILink,
            type: "GET",
            data: { "kodeHotel": kodeHotel, "kodeRuangan": kodeRuangan },
            beforeSend: () => {
                $("#message").html("Sedang menghapus data...");
                tableBody.removeChild(row);
            },
            success: response => {
                $("#message").html(response.message);
            }
        })
    })
})