$(document).ready(function () {
  $("select").formSelect();
});

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

// Update Operation
const editButtons = document.querySelectorAll(".edit-btn");

const isiForm = row => {
    let idTransaksi = row.children[1].innerHTML;
    let kodePasien = row.children[2].innerHTML;
    let tanggalMasuk = row.children[5].innerHTML;
    let totalBiaya = row.children[6].innerHTML;
    let status = row.children[7].innerHTML;
    $("#idTransaksi").attr("placeholder", idTransaksi);
    $("#nik").attr("placeholder", kodePasien);
    $("#tanggal_masuk").attr("placeholder", tanggalMasuk);
    $("#biaya").attr("placeholder", totalBiaya);
    $("#status-select").val(status);
    $("select").formSelect();

    let date = new Date();
    let today = date.toISOString().slice(0, 10);
    let currentTime = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    let currentDateTime = today + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    $("#tanggal_bayar").attr("placeholder", today);
    $("#waktu_bayar").attr("placeholder", currentTime);
    return [today, currentTime, currentDateTime];
}

editButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        let idTransaksi = row.children[1].innerHTML;
        const dateTime = isiForm(row);
        let currentDate = dateTime[0];
        let currentTime = dateTime[1];
        let currentDateTime = dateTime[2];
        $("#kirim-btn").click(() => {
            let status = $("#status-select").val();
            let APILink = location.origin + "/api/transaksiRSAPI/";
            $.ajax({
                url: APILink,
                type: "POST",
                headers: { "X-CSRFToken": csrftoken },
                data: {
                    idTransaksi: idTransaksi,
                    tanggalBayar: currentDate,
                    waktuBayar: currentDateTime,
                    status: status,
                },
                beforeSend: () => {
                    $("#message").html("Sedang mengirim data...");
                },
                success: (response) => {
                    $("#message").html(response.message);
                    row.children[3].innerHTML = currentDate;
                    row.children[4].innerHTML = currentTime;
                    row.children[7].innerHTML = status;
                },
            });
        });
    })
})

// DELETE Operation
const deleteButtons = document.querySelectorAll(".delete-btn");
const tableBody = document.querySelector("#table-body");

deleteButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        let idTransaksi = row.children[1].innerHTML;
        let APILink = location.origin + "/api/transaksiRSAPI/";
        $.ajax({
            url: APILink,
            type: "GET",
            data: { "idTransaksi": idTransaksi },
            beforeSend: () => {
                $("#message").html("Sedang menghapus data...");
                tableBody.removeChild(row);
            },
            success: response => {
                $("#message").html(response.message);
            }
        })
    })
})