$(document).ready(function () {
  $("select").formSelect();
  $(".datepicker").datepicker({ format: "yyyy-mm-dd" });
});

$("#kodeRS-select").change(() => {
	let kodeRS = $("#kodeRS-select").val();
	let APILink = location.origin + "/api/kodeRuanganAPI/";
	$.ajax({
		url: APILink,
		type: "GET",
		data: { "kodeRS": kodeRS },
		beforeSend: () => {
			$("#kodeRuangan-select").empty();
			$("#kodeRuangan-select").append(
			`<option value="" disabled selected>Sedang mengambil data...</option>`
			);
			$("select").formSelect();
		},
		success: response => {
			let data = response.data;
			$("#kodeRuangan-select").empty();
			if (data.length == 0) {
				$("#kodeRuangan-select").append(
				`<option value="" disabled selected>Belum ada ruangan...</option>`
				);
			} else {
				$("#kodeRuangan-select").append(
				`<option value="" disabled selected>Pilih...</option>`
				);
				data.forEach(kode => {
					$("#kodeRuangan-select").append(
						`<option value="${kode[0]}">${kode[0]}</option>`
					);
				})
			}
			$("select").formSelect();
		}
	})
});

$("#kodeRuangan-select").change(() => {
	let kodeRS = $("#kodeRS-select").val();
	let kodeRuangan = $("#kodeRuangan-select").val();
	let APILink = location.origin + "/api/kodeBedAPI2/";
	$.ajax({
		url: APILink,
		type: "GET",
		data: { "kodeRS": kodeRS, "kodeRuangan": kodeRuangan },
		beforeSend: () => {
			$("#kodeBed-select").empty();
			$("#kodeBed-select").append(
				`<option value="" disabled selected>Sedang mengambil data...</option>`
			);
			$("select").formSelect();
		},
		success: response => {
			let data = response.data;
			$("#kodeBed-select").empty();
			if (data.length == 0) {
				$("#kodeBed-select").append(
				`<option value="" disabled selected>Belum ada bed...</option>`
				);
			} else {
				$("#kodeBed-select").append(
					`<option value="" disabled selected>Pilih...</option>`
				);
				data.forEach((kode) => {
					$("#kodeBed-select").append(
						`<option value="${kode[0]}">${kode[0]}</option>`
					);
				});
			}
			$("select").formSelect();
		}
	})
});

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

const buatReservasiRS = form => {
	let APILink = location.origin + "/api/reservasiRSAPI/";
	$.ajax({
		url: APILink,
		type: "POST",
		headers: { "X-CSRFToken": csrftoken },
		data: form,
		beforeSend: () => {
			$("#message").html("Sedang mengirim data...");
		},
		success: response => {
			$("#message").html(response.message);
			window.location.href = location.origin + "/reservasiRS/listReservasiRS";
		}
	});
}

$("#btn-buat").click(() => {
	buatReservasiRS($("#reservasiRS-form").serialize());
});


// Update Operation

const editButtons = document.querySelectorAll(".edit-btn");
editButtons.forEach(btn => {
	btn.addEventListener('click', () => {
		let row = btn.parentElement.parentElement;
		const nik = row.children[1].innerHTML;
		const tanggalMasuk = row.children[2].innerHTML;
		const kodeRS = row.children[4].innerHTML;
		const kodeRuangan = row.children[5].innerHTML;
		const kodeBed = row.children[6].innerHTML;
		$("#nik").attr("placeholder", nik);
		$("#kodeRS").attr("placeholder", kodeRS);
		$("#tanggal_masuk").attr("placeholder", tanggalMasuk);
		$("#kodeRuangan").attr("placeholder", kodeRuangan);
		$("#kodeBed").attr("placeholder", kodeBed);
		$("#tanggal_keluar").val("");
		let form = {
			"nik": nik,
			"tanggal_masuk": tanggalMasuk,
		}
		$("#kirim-btn").click(() => {
			const tanggalKeluar = $("#tanggal_keluar").val();
			form["tanggal_keluar"] = tanggalKeluar;

			let APILink = location.origin + "/api/updateReservasiRSAPI/";
			$.ajax({
				url: APILink,
				type: "POST",
				headers: { "X-CSRFToken": csrftoken },
				data: form,
				beforeSend: () => {
					$("#message").html("Sedang mengirim data...");
				},
				success: response => {
					$("#message").html(response.message);
					row.children[3].innerHTML = tanggalKeluar;
				}
			});
		});
	})
})

// DELETE Operation
let deleteButtons = document.querySelectorAll(".delete-btn");
const tableBody = document.querySelector("#table-body");

deleteButtons.forEach(btn => {
	btn.addEventListener('click', () => {
		let row = btn.parentElement.parentElement;
		const nik = row.children[1].innerHTML;
		const tanggalMasuk = row.children[2].innerHTML;
		let APILink = location.origin + "/api/deleteReservasiAPI/";
		$.ajax({
			url: APILink,
			type: "GET",
			data: { "nik": nik, "tanggal_masuk": tanggalMasuk },
			beforeSend: () => {
				$("#message").html("Sedang menghapus data...");
				tableBody.removeChild(row);
			},
			success: response => {
				$("#message").html(response.message);
			}
		})
	})
})