$(document).ready(function () {
  $(".modal").modal();
});

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

// CREATE Operation on Jadwal Dokter
const tambahButtons = document.querySelectorAll(".tambah-btn");
const tableBody = document.querySelector("#table-body");

tambahButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        const kodeFaskes = row.children[1].innerHTML;
        const shift = row.children[2].innerHTML;
        const tanggal = row.children[3].innerHTML;
        const form = {"kode_faskes": kodeFaskes, "shift": shift, "tanggal": tanggal}
        $("#kirim-btn").click(() => {
            const APILink = location.origin + "/api/jadwalDokterAPI/";
            $.ajax({
                url: APILink,
                type: "POST",
                headers: { "X-CSRFToken": csrftoken },
                data: form,
                beforeSend: () => {
                    $("#message").html("Sedang membuat jadwal...");
                    tableBody.removeChild(row);
                },
                success: (response) => {
                    $("#message").html(response.message);
                    window.location.href = location.origin + "/jadwalDokter/listJadwal/";
                },
            });
        });
    })
});


// READ Operation on Jadwal Dokter