const generateHTML = (data, counter) => {
    const HTMLText = `
    <tr>
        <td>${counter}</td>
        <td>${data[0]}</td>
        <td>${data[1]}</td>
        <td>${data[2]}</td>
        <td>${data[3]}</td>
        <td>${data[4]}</td>
        <td>${data[5]}</td>
    </tr>
    `;
    return HTMLText;
}

$(document).ready(() => {
    const APILink = location.origin + "/api/jadwalDokterAPI/";
    $.ajax({
        url: APILink,
        type: "GET",
        beforeSend: () => {
            $("#message").html("Sedang mengambil data jadwal dokter...");
        },
        success: (response) => {
            const dataJadwal = response.data;
            console.log(response);
            const message = response.message;
            $("#message").html(message);
            let counter = 1;
            dataJadwal.forEach(data => {
                const HTMLText = generateHTML(data, counter);
                $("#table-jadwal").append(HTMLText);
                counter++;
            });
        }
    });
})