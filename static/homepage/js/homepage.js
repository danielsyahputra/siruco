$(document).ready(function () {
  $(".collapsible").collapsible();
});

const slider = document.querySelector(".slider");
M.Slider.init(slider, {
  indicators: false,
  interval: 4000,
  height: 500
});

$(".btn-register").click(() => {
  console.log('CLICK');
  window.location.href = location.origin + '/pengguna/register/';
});