const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

// CREATE Operation
const buatRumahSakit = (form) => {
    let APILink = location.origin + "/api/rumahSakitAPI/";
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            window.location.href =
              location.origin + "/rumahSakit/listRumahSakit/";
        }
    });
};

$("#btn-buat").click(() => {
    let form = $("#rumahSakit-form").serialize();
    buatRumahSakit(form);
});

// Update Operation
const editButtons = document.querySelectorAll(".edit-btn");

editButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        const kodeFaskes = row.children[1].innerHTML;
        $("#kodeFaskes").attr("placeholder", kodeFaskes);
        $("#kodeFaskes").val(kodeFaskes);
        $("#kirim-btn").click(() => {
            let form = $("#rumahSakit-form").serialize();
            let APILink = location.origin + "/api/rumahSakitAPI/";
            $.ajax({
                url: APILink,
                type: "GET",
                data: form,
                beforeSend: () => {
                    $("#message").html("Sedang mengirim data...");
                },
                success: response => {
                    const rujukan = response.rujukan;
                    $("#message").html(response.message);
                    if (rujukan == "1") {
                        row.children[2].innerHTML = `<i class="material-icons">check</i>`;
                    } else {
                        row.children[2].innerHTML = `<i class="material-icons">close</i>`;
                    }
                },
            });
        });
    })
})