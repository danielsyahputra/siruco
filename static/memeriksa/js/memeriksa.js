$(document).ready(function () {
    $("select").formSelect();
});

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;
const tambahButtons = document.querySelectorAll(".tambah-btn");

const generateNIK = () => {
    let APILink = location.origin + "/api/appointmentAPI/";
    $.ajax({
        url: APILink,
        type: "GET",
        beforeSend: () => {
            $("#message-modal").html("Sedang mengambil data NIK Pasien...");
        },
        success: response => {
            const niks = response.data;
            niks.forEach(nik => {
                const option = `<option value="${nik[0]}">${nik[1]}: ${nik[0]}</option>`;
                $("#nik-select").append(option);
            })
            $("select").formSelect();
            $("#message-modal").html("Silahkan masukkan NIK Pasien yang ingin anda daftarkan...");
        }
    });
}


// CREATE OPERATION
tambahButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        const noSTR = row.children[1].innerHTML;
        const idDokter = row.children[2].innerHTML;
        const kodeFaskes = row.children[3].innerHTML;
        const shift = row.children[4].innerHTML;
        const tanggal = row.children[5].innerHTML;
        $("#no_str").attr("placeholder", noSTR);
        $("#username").attr("placeholder", idDokter);
        $("#kode_faskes").attr("placeholder", kodeFaskes);
        $("#shift").attr("placeholder", shift);
        $("#tanggal").attr("placeholder", tanggal);
        generateNIK();
        $("#kirim-btn").click(() => {
            let APILink = location.origin + "/api/appointmentAPI/";
            const nik = $("#nik-select").val();
            let form = {
                "nik": nik,
                "nostr": noSTR,
                "dokter": idDokter,
                "kode_faskes": kodeFaskes,
                "shift": shift,
                "tanggal": tanggal
            }
            $.ajax({
                url: APILink,
                type: "POST",
                headers: { "X-CSRFToken": csrftoken },
                data: form,
                beforeSend: () => {
                    $("#message").html("Sedang mengirim data...");
                },
                success: (response) => {
                    $("#message").html(response.message);
                    window.location.href =
                      location.origin + "/memeriksa/list_appointment/";
                },
            });
        });
    })
});

// Update Operation
const editButtons = document.querySelectorAll(".edit-btn");

editButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        const nikPasien = row.children[1].innerHTML;
        const idDokter = row.children[2].innerHTML;
        const kodeFaskes = row.children[3].innerHTML;
        const shift = row.children[4].innerHTML;
        const tanggal = row.children[5].innerHTML;
        $("#nik").attr("placeholder", nikPasien);
        $("#username").attr("placeholder", idDokter);
        $("#kode_faskes").attr("placeholder", kodeFaskes);
        $("#shift").attr("placeholder", shift);
        $("#tanggal").attr("placeholder", tanggal);
        $("#kirim-btn").click(() => {
            let APILink = location.origin + "/api/appointmentAPI2/";
            const rekomendasi = $('#rekomendasi').val();
            let form = {
                "nik": nikPasien,
                "dokter": idDokter,
                "kode_faskes": kodeFaskes,
                "shift": shift,
                "tanggal": tanggal,
                "rekomendasi": rekomendasi
            }
            $.ajax({
                url: APILink,
                type: "POST",
                headers: { "X-CSRFToken": csrftoken },
                data: form,
                beforeSend: () => {
                    $("#message").html("Sedang mengirim data...");
                },
                success: response => {
                    $("#message").html(response.message);
                    row.children[6].innerHTML = rekomendasi;
                }
            });
        });
    })
})

// Delete Operation
const deleteButtons = document.querySelectorAll(".delete-btn");
const tableBody = document.querySelector("#table-body");

deleteButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        const nikPasien = row.children[1].innerHTML;
        const idDokter = row.children[2].innerHTML;
        const kodeFaskes = row.children[3].innerHTML;
        const shift = row.children[4].innerHTML;
        const tanggal = row.children[5].innerHTML;
        let APILink = location.origin + "/api/appointmentAPI2/";
        let form = {
            "nik": nikPasien,
            "dokter": idDokter,
            "kode_faskes": kodeFaskes,
            "shift": shift,
            "tanggal": tanggal
        }
        $.ajax({
            url: APILink,
            type: "GET",
            data: form,
            beforeSend: () => {
                $("#message").html("Sedang menghapus data...");
                tableBody.removeChild(row);
            },
            success: response => {
                $("#message").html(response.message);
            }
        });
    })
})