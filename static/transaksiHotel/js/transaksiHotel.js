$(document).ready(function () {
  $("select").formSelect();
});

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

const editButtons = document.querySelectorAll(".edit-btn");

const isiForm = row => {
    let idTransaksi = row.children[1].innerHTML;
    let kodePasien = row.children[2].innerHTML;
    let tanggalBayar = row.children[3].innerHTML;
    let waktuBayar = row.children[4].innerHTML;
    let totalBiaya = row.children[5].innerHTML;
    let statusBayar = row.children[6].innerHTML;
    $("#nik").val(kodePasien);
    $("#idTransaksi").val(idTransaksi);
    $("#biaya").val(totalBiaya);
    $("#status-select").val(statusBayar);
    $("select").formSelect();

    if (tanggalBayar === "" || waktuBayar == "") {
        let date = new Date();
        let today = date.toISOString().slice(0, 10);
        let currentTime = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        let currentDateTime = today + " " + currentTime;
        $("#tanggal_bayar").val(today);
        $("#waktu_bayar").val(currentDateTime);
    } else {
        $("#tanggal_bayar").val(tanggalBayar);
        $("#waktu_bayar").val(waktuBayar);
    }
};

const updateTransaksiHotel = (form, row) => {
    let APILink = location.origin + "/api/transaksiHotelAPI/";
    let statusBayar = $("#status-select").val();
    let tanggalBayar = $("#tanggal_bayar").val();
    let waktuBayar = $("#waktu_bayar").val();
    $.ajax({
        url: APILink,
        type: "POST",
        headers: { "X-CSRFToken": csrftoken },
        data: form,
        beforeSend: () => {
            $("#message").html("Sedang mengirim data...");
        },
        success: response => {
            $("#message").html(response.message);
            row.children[6].innerHTML = statusBayar;
            row.children[3].innerHTML = tanggalBayar;
            row.children[4].innerHTML = waktuBayar;
        }
    })
};

editButtons.forEach(btn => {
    btn.addEventListener('click', () => {
        let row = btn.parentElement.parentElement;
        isiForm(row);
        $("#kirim-btn").click(() => {
            $("#idTransaksi").attr("disabled", false);
            $("#tanggal_bayar").attr("disabled", false);
            $("#waktu_bayar").attr("disabled", false);
            let form = $("#transaksiHotel-form").serialize();
            updateTransaksiHotel(form, row);
        });
    });
})