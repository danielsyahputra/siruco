from django.urls import path

from . import views

app_name = 'transaksiMakan'

urlpatterns = [
    path('listTransaksiMakan/', views.listTransaksiMakan, name='listTransaksiMakan'),
    path('buatTransaksiMakan/', views.buatTransaksiMakan, name='buatTransaksiMakan'),
]