from django.shortcuts import render

from database import MyDatabase

def listTransaksiMakan(request):
    db = MyDatabase()
    context = {}
    query = "SELECT * FROM TRANSAKSI_MAKAN"
    context['datas'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'transaksiMakan/list_transaksi_makan.html', context)

def buatTransaksiMakan(request):
    db = MyDatabase()
    context = {}
    query = "SELECT idtransaksi FROM TRANSAKSI_HOTEL"
    context['datas'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'transaksiMakan/buat_transaksi_makan.html', context)