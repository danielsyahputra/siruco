from django.apps import AppConfig


class RuanganrsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ruanganRS'
