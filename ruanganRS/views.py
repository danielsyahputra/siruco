from django.shortcuts import render
from database import MyDatabase

def list_ruanganRS(request):
    db = MyDatabase()
    context = {}
    query = f"SELECT * FROM RUANGAN_RS"
    context['rooms'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'ruanganRS/list_ruanganRS.html', context)

def list_bedRS(request):
    db = MyDatabase()
    context = {}
    query = f"SELECT * FROM BED_RS"
    context['beds'] = db.query(query=query, fetchAll=True)
    db.close()
    return render(request, 'ruanganRS/list_bedRS.html', context)

def buat_ruanganRS(request):
    db = MyDatabase()
    context = {}
    query = f"SELECT kode_faskes FROM RUMAH_SAKIT"
    result = db.query(query=query, fetchAll=True)
    db.close()
    context['kodeRS'] = result
    return render(request, 'ruanganRS/buat_ruanganRS.html', context)

def buat_bedRS(request):
    db = MyDatabase()
    context = {}
    query = f"SELECT kode_faskes FROM RUMAH_SAKIT"
    result = db.query(query=query, fetchAll=True)
    db.close()
    context['kodeRS'] = result
    return render(request, 'ruanganRS/buat_bedRS.html', context)