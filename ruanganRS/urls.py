from django.urls import path

from . import views

app_name = 'ruanganRS'

urlpatterns = [
    path('list_ruanganRS/', views.list_ruanganRS, name='list_ruanganRS'),
    path('list_bedRS/', views.list_bedRS, name='list_bedRS'),
    path('buat_ruanganRS/', views.buat_ruanganRS, name='buat_ruanganRS'),
    path('buat_bedRS/', views.buat_bedRS, name='buat_bedRS')
]